package com.jankariportal;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.L;
import com.jankariportal.ui.attendance.AttendanceFragment;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class AttendanceLogActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<AttendanceLog> logList = new ArrayList<>();
    private LogAdapter adapter;
    View progressLayout;

    private String empid;
    String date, timein, address1, address2, timeout, inimg, outimg;
    String slongitude, slatitude, elongitude, elatitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_log);
        setTitle("Attendance Log");
        recyclerView = findViewById(R.id.recycler_view);
        adapter = new LogAdapter(logList);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.divider));
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(adapter);
        progressLayout = findViewById(R.id.progress_layout);
        progressLayout.setVisibility(View.VISIBLE);
        SharedPreferences sharedPreferences = getSharedPreferences("Jankari", MODE_PRIVATE);
        empid = sharedPreferences.getString("userid", "");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new GetValuesFromJSON().execute();
            }
        },500);
    }

    public String prepareAddress(double longti, double lati) {
        String data = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lati, longti, 1);
            data = addresses.get(0).getAddressLine(0);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private class GetValuesFromJSON extends AsyncTask<Void, Void, Void> {
        JSONObject jsonObject;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            logList.clear();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            JSONHandler jsonHandler = new JSONHandler();
            String jsonStr = jsonHandler.makeJSONCall("http://api.jankari.org.in/api/Employee/GetAttendanceLogList?whr=where%20Employee.empid='" + empid + "'");
            if (jsonStr != null) {
                try {
                    JSONArray jsonArray = new JSONArray(jsonStr);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);

                        date = jsonObject.getString("date").substring(0, 10);
                        timein = jsonObject.getString("starttime");
                        timeout = jsonObject.getString("endtime");
                        slongitude = jsonObject.getString("startloclongitude");
                        slatitude = jsonObject.getString("startloclatitude");
                        elongitude = jsonObject.getString("endloclongitude");
                        elatitude = jsonObject.getString("endloclatitude");
                        inimg = jsonObject.getString("photoin");
                        outimg = jsonObject.getString("photoout");
                        if (slatitude == "null" && slongitude == "null") {

                        } else {
                            address1 = prepareAddress(Double.parseDouble(slongitude), Double.parseDouble(slatitude));
                        }
                        if (elatitude == "null" && elongitude == "null") {

                        } else {
                            address2 = prepareAddress(Double.parseDouble(elongitude), Double.parseDouble(elatitude));
                        }
                        AttendanceLog attendanceLog = new AttendanceLog(date, address1, address2, inimg, outimg, timein, timeout);
                        logList.add(attendanceLog);

                        SharedPreferences sharedPreferences = getSharedPreferences("Jankari", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("date", date);
                        editor.putString("intime", timein);
                        editor.putString("outtime", timeout);
                        editor.commit();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(), "PROBLEM!!!", Toast.LENGTH_SHORT).show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            super.onPostExecute(aVoid);
            adapter.notifyDataSetChanged();
            progressLayout.setVisibility(View.GONE);
        }
    }

}
