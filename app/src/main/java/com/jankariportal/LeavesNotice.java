package com.jankariportal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class LeavesNotice extends AppCompatActivity {
    private TextView type1,type2,type3;
    private TextView actual1,actual2,actual3;
    private TextView used1,used2,used3;
    private TextView pending1,pending2,pending3;
    String empid;
    SharedPreferences sharedPreferences ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaves_notice);
        setTitle("Leaves History");
        sharedPreferences = this.getSharedPreferences("Jankari",MODE_PRIVATE);
        empid = sharedPreferences.getString("userid","");
        type1 = findViewById(R.id.leaveType1);
        type2 = findViewById(R.id.leaveType2);
        type3 = findViewById(R.id.leaveType3);

        actual1 = findViewById(R.id.actual1);
        actual2 = findViewById(R.id.actual2);
        actual3 = findViewById(R.id.actual3);

        used1 = findViewById(R.id.used1);
        used2 = findViewById(R.id.used2);
        used3 = findViewById(R.id.used3);

        pending1 = findViewById(R.id.pending1);
        pending2 = findViewById(R.id.pending2);
        pending3 = findViewById(R.id.pending3);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        new GetLeavesValues().execute();
        getLeaveData();
    }
    public void getLeaveData(){
        RequestQueue queue = Volley.newRequestQueue(this);
        final String url = "http://api.jankari.org.in/api/Employee/GetEmployeeLeaveData/"+empid;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray array = new JSONArray(response);
                    for (int i = 0;i<array.length();i++){
                        JSONObject jsonObject = array.getJSONObject(i);
                        if (i == 0){
                            used1.setText(String.valueOf(jsonObject.getInt("usedleave")));
                            pending1.setText(String.valueOf(jsonObject.getInt("remainingleave")));
                        }
                        if (i == 1){
                            used2.setText(String.valueOf(jsonObject.getInt("usedleave")));
                            pending2.setText(String.valueOf(jsonObject.getInt("remainingleave")));
                        }
                        if (i == 2){
                            used3.setText(String.valueOf(jsonObject.getInt("usedleave")));
                            pending3.setText(String.valueOf(jsonObject.getInt("remainingleave")));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Something Went Wrong :(", Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(stringRequest);
    }
    private class GetLeavesValues extends AsyncTask<Void,Void,Void>{
        HashMap<String, Integer> val = new HashMap<>();
        @Override
        protected Void doInBackground(Void... voids) {
            String url = "http://api.jankari.org.in/api/JankariPortal/LeaveCountList";
            JSONHandler jsonHandler = new JSONHandler();
            String jsonStr = jsonHandler.makeJSONCall(url);
            try {
                JSONArray jsonArray = new JSONArray(jsonStr);
                for (int i = 0;i<jsonArray.length();i++){
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    val.put(jsonObject.getString("leavetype"),jsonObject.getInt("leavecount1"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            List<String> type = new ArrayList<>();
            List<Integer> value = new ArrayList<>();
            for (Map.Entry<String,Integer> entry : val.entrySet()){
                type.add(entry.getKey());
                value.add(entry.getValue());
            }
            type1.setText(type.get(0));
            type2.setText(type.get(1));
            type3.setText(type.get(2));

            actual1.setText(String.valueOf(value.get(0)));
            actual2.setText(String.valueOf(value.get(1)));
            actual3.setText(String.valueOf(value.get(2)));
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

}
