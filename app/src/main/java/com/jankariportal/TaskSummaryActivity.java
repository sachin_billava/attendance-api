package com.jankariportal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.Time;
import android.util.Log;
import android.util.TimeUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jankariportal.ui.task.TaskLogFragment;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class TaskSummaryActivity extends AppCompatActivity {
    TextView emp,tasks,totalTime,date;
    RecyclerView recyclerView;
    String idate;
    TaskSummaryAdapter adapter;
    String empid;
    SharedPreferences sharedPreferences;
    List<TaskSummary> taskSummaryList;
    SwipeRefreshLayout swipeRefreshLayout;
    FloatingActionButton floatingActionButton;
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_task_summary);
        setTitle("Task Summary");
        emp = findViewById(R.id.emp_name);
        tasks = findViewById(R.id.task_count);
        totalTime = findViewById(R.id.total_time);
        date = findViewById(R.id.task_date);
        sharedPreferences = this.getSharedPreferences("Jankari",MODE_PRIVATE);
        empid = sharedPreferences.getString("userid","");
        swipeRefreshLayout = findViewById(R.id.swipe_continer);
        recyclerView = findViewById(R.id.summary_recycler);
        taskSummaryList = new ArrayList<>();
        idate = getIntent().getExtras().getString("date");
        tasks.setText("Tasks : "+getIntent().getExtras().getInt("count"));
        date.setText("Date : "+idate);
        floatingActionButton = findViewById(R.id.fab);
        adapter = new TaskSummaryAdapter(taskSummaryList);
//        Log.e("Date",idate);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override public void run() {
                        // Stop animation (This will be after 3 seconds)
                        taskSummaryList.clear();
                        new GetValuesFromJson(idate).execute();
                        Toast.makeText(TaskSummaryActivity.this, "Record Refreshed!", Toast.LENGTH_LONG).show();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 3000);
            }
        });
        swipeRefreshLayout.setColorSchemeColors(
                getResources().getColor(android.R.color.holo_blue_bright),
                getResources().getColor(android.R.color.holo_green_light),
                getResources().getColor(android.R.color.holo_orange_light),
                getResources().getColor(android.R.color.holo_red_light)
        );
        new GetValuesFromJson(idate).execute();
        floatingActionButton.setVisibility(View.GONE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TaskSummaryActivity.this,AddTaskActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    public class GetValuesFromJson extends AsyncTask<Void,Void,Void>{
        String date;

        public GetValuesFromJson(String date) {
            this.date = date;
        }

        @SuppressLint("WrongThread")
        @Override
        protected Void doInBackground(Void... voids) {
            JSONHandler jsonHandler = new JSONHandler();
            String jsonStr = jsonHandler.makeJSONCall("http://api.jankari.org.in/api/JankariPortal/GetUserDailyReportList?whr= where UserDailyReport.reportdate='"+date+"' And UserDailyReport.userid='"+empid+"'");
            Log.e("date",date);
            if (jsonStr != null){
                try {
                    String stime,etime,desc,status,name = "";
                    JSONArray jsonArray = new JSONArray(jsonStr);
                    long finalTime = 0;
//                    Log.e("array", String.valueOf(jsonArray));
                    for (int i = 0;i<jsonArray.length();i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
                        status = "";
                        int sts;
                        stime = jsonObject.getString("starttime").substring(0,5);
                        etime = jsonObject.getString("endtime").substring(0,5);
                        desc = jsonObject.getString("description");
                        if (desc.length() >= 25){
                            desc = desc.substring(0,25) + "...";
                        }
                        sts = jsonObject.getInt("status");
                        name = jsonObject.getString("fullname");
                        Date start = simpleDateFormat.parse(stime);
                        Date end = simpleDateFormat.parse(etime);
                        long Diff = (end.getTime() - start.getTime())/1000;
                        finalTime = finalTime+Diff;
                        if (sts == 0){
                            status = "Pending";
                        }
                        if (sts == 1){
                            status = "Completed";
                        }
                        TaskSummary taskSummary = new TaskSummary(stime,etime,desc,status);
                        taskSummaryList.add(taskSummary);

                    }
                    totalTime.setText("Total Time : "+TimeUnit.SECONDS.toHours(finalTime));
                    emp.setText(name);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            adapter.notifyDataSetChanged();
        }
    }

}
