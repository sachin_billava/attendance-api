package com.jankariportal;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.os.Handler;
import android.transition.Explode;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private AppBarConfiguration mAppBarConfiguration;
    boolean doubleBackToExitPressedOnce = false;
    TextView txtUserName, txtEmail, txtLogOut;
    String emp = "", email = "", profilePic, empid;

    String role = "";
    boolean flag = false;//Todo change flag = true to disable EmployeeAttendance
    DrawerLayout drawer;
    CircleImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        userDetails();


        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("Jankari", MODE_PRIVATE);
        emp = sharedPreferences.getString("fullname", "");
        email = sharedPreferences.getString("email", "");
        empid = sharedPreferences.getString("userid", "");
        profilePic = sharedPreferences.getString("empphoto","");


        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.

        View headerView = navigationView.getHeaderView(0);
        txtUserName = headerView.findViewById(R.id.txtUserName);
        imageView = headerView.findViewById(R.id.imageView);
//        txtEmail = headerView.findViewById(R.id.txtEmail);

        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_dashboard, R.id.nav_logout)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        txtUserName.setText("Welcome, " + emp);
        imageView.setVisibility(View.VISIBLE);
        if (profilePic.equals(null) || profilePic.equals("null") || profilePic.equals("")) {
//            takeImage.setImageResource(R.mipmap.ic_launcher);
        } else {
            Picasso.get().load(getResources().getString(R.string.domain_url) + getResources().getString(R.string.get_profilepic_path) + profilePic)
                    .into(imageView);
        }
        contextOfApplication = getApplicationContext();
    }

    // a static variable to get a reference of our application context
    public static Context contextOfApplication;

    public static Context getContextOfApplication() {
        return contextOfApplication;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(Gravity.LEFT | GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
//
            this.finish();
            return;
        }
//
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    private void userDetails() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String urlEmpDetails = "http://api.jankari.org.in/api/JankariPortal/GetAPPEmployeeDetails/" + empid;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, urlEmpDetails, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //show dialog

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        emp = jsonObject.getString("empname");
                        email = jsonObject.getString("offemail");
                        profilePic = jsonObject.getString("empphoto");

                        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("Jankari", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("fullname", emp);
                        editor.putString("empphoto", profilePic);
                        editor.commit();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        Log.e("item", String.valueOf(menuItem.getItemId()));
        switch (id) {
            case R.id.nav_logout:
                showPopup();
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showPopup() {
        androidx.appcompat.app.AlertDialog.Builder alert = new androidx.appcompat.app.AlertDialog.Builder(MainActivity.this);
        alert.setMessage("Are you sure?")
                .setPositiveButton("Logout", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                        logout(); // Last step. Logout function

                    }
                }).setNegativeButton("Cancel", null);
        androidx.appcompat.app.AlertDialog alert1 = alert.create();
        alert1.show();
    }

    private void logout() {
        Intent mIntent = new Intent(this, LoginActivity.class); //'this' is Activity A
        mIntent.putExtra("FROM_ACTIVITY", "A");
        startActivity(mIntent);
        SharedPreferences preferences = getSharedPreferences("Jankari", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
        finish();
    }


}
