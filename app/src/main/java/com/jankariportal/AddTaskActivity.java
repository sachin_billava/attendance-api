package com.jankariportal;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class AddTaskActivity extends AppCompatActivity {
    EditText reportDate,description,startTime,endTime;
    private Spinner status;
    Button submit;
    Calendar calendar;
    String report;
    String pgid,roleid;
    String empid;
    View progressLayout;
    String cdate;
    SharedPreferences sharedPreferences;
    Context context;
    @SuppressLint("SimpleDateFormat")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);
        setTitle("Add Task");
        context = getApplicationContext();
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        reportDate = findViewById(R.id.reportDate);
        description = findViewById(R.id.description);
        startTime = findViewById(R.id.startTime);
        endTime = findViewById(R.id.endTime);
        status = findViewById(R.id.status);
        submit = findViewById(R.id.submit);
        calendar = Calendar.getInstance();
        progressLayout = findViewById(R.id.progress_layout);
        selectSpinnerData(status);
        sharedPreferences = this.getSharedPreferences("Jankari", Context.MODE_PRIVATE);
        empid = sharedPreferences.getString("userid","");
        if (getIntent().hasExtra("reportdate")){
            report = getIntent().getExtras().getString("reportdate");
        }
        cdate = new SimpleDateFormat("dd-MM-yyyy").format(new Date());

        reportDate.setText(cdate);
        final DatePickerDialog.OnDateSetListener reportDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR,year);
                calendar.set(Calendar.MONTH,month);
                calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                String format = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
                String date = simpleDateFormat.format(calendar.getTime());
                reportDate.setText(date);
            }
        };
        final TimePickerDialog.OnTimeSetListener startTimeListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                calendar.set(Calendar.MINUTE,minute);
                String format = "HH:mm";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
                String time = simpleDateFormat.format(calendar.getTime());
                startTime.setText(time);
            }
        };
        final TimePickerDialog.OnTimeSetListener endTimeListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                calendar.set(Calendar.MINUTE,minute);
                String format = "HH:mm";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
                String time = simpleDateFormat.format(calendar.getTime());
                endTime.setText(time);
            }
        };

        reportDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(AddTaskActivity.this,reportDateSetListener,calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        startTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(AddTaskActivity.this,startTimeListener,calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE),false).show();
            }
        });
        endTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(AddTaskActivity.this,endTimeListener,calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE),false).show();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressLayout.setVisibility(View.VISIBLE);
                checkFields();
            }
        });
        description.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
        if (report != null){
            new GetValuesFromJSON().execute();
        }
//        Lg
        new GetPGID().execute();
        new GetRoleID().execute();
    }
    private class GetValuesFromJSON extends AsyncTask<Void,Void,Void>{
    String stime,etime,desc,reporttext;
    int sts;
        @Override
        protected Void doInBackground(Void... voids) {
            JSONHandler jsonHandler = new JSONHandler();
            String jsonStr = jsonHandler.makeJSONCall("http://api.jankari.org.in/api/JankariPortal/GetUserDailyReportList?whr= where UserDailyReport.reportdate='"+report+"' And UserDailyReport.userid='"+empid+"'");
            if (jsonStr != null){
                try {
                    JSONArray jsonArray = new JSONArray(jsonStr);
                   JSONObject jsonObject = jsonArray.getJSONObject(0);
                   stime = jsonObject.getString("starttime");
                   etime = jsonObject.getString("endtime");
                   reporttext = jsonObject.getString("reportdate");
                   desc = jsonObject.getString("description");
                   sts = jsonObject.getInt("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            reportDate.setText(reporttext);
            startTime.setText(stime);
            endTime.setText(etime);
            description.setText(desc);
            if (sts == 0){
                status.setSelection(2);
            }else if (sts == 1){
                status.setSelection(1);
            }else{
                status.setSelection(0);
            }

        }
    }
    private void checkFields() {
        boolean confirm = false;
        if (startTime.getText().toString().length() != 0){
            if (endTime.getText().toString().length() != 0){
                if (status.getSelectedItem() != "Select Status"){
                    if (description.getText().toString().length() != 0){
                        confirm = true;
                        if (report != null){
                            if (confirm){
                                addTask();
                            }
                        }else if (confirm)addTask();
                    }else{
                        description.setError("Please Enter Details..");
                    }
                }else{
                    Toast.makeText(this,"Please Select Correct Status..",Toast.LENGTH_SHORT).show();
                }
            }   else{
                endTime.setError("Please Select End Time...");
            }
        }else{
            startTime.setError("Please Select Start Time...");
        }
    }

    private class GetRoleID extends AsyncTask<Void,Void,Void>{
        @Override
        protected Void doInBackground(Void... voids) {
            String roleUrl = "http://api.jankari.org.in/api/JankariPortal/GetProgramWiseEmployeeRole/"+pgid+"/"+empid;
            JSONHandler jsonHandler = new JSONHandler();
            String jsonStr = jsonHandler.makeJSONCall(roleUrl);
            try {
                JSONObject jsonObject = new JSONObject(jsonStr);
                roleid = jsonObject.getString("role");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class GetPGID extends AsyncTask<Void,Void,Void>{
        @Override
        protected Void doInBackground(Void... voids) {
            String pgUrl = "http://api.jankari.org.in/api/JankariPortal/GetUserProgramLoginList?whr=where UserMaster.userid='"+empid+"'";
            JSONHandler jsonHandler = new JSONHandler();
            String jsonStr = jsonHandler.makeJSONCall(pgUrl);
            try {
                JSONArray jsonArray = new JSONArray(jsonStr);
                for (int i = 0; i<jsonArray.length();i++){
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    pgid = jsonObject.getString("pgid");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private void addTask() {

        String url = "http://api.jankari.org.in/api/JankariPortal/AddUserDailyTask/"+empid;
        try {
            String report,start,end,desc,createdon;
            int sts;
            String old =reportDate.getText().toString().trim();
            if (cdate.equalsIgnoreCase(old)){
                report = old;
            }else{
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date nd = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").parse(old);
                report = sdf.format(nd);
            }

            start = startTime.getText().toString().trim();
            end = endTime.getText().toString().trim();
            desc = description.getText().toString().trim();
            if (status.getSelectedItem() == "Completed"){
                sts = 1;
            }else{
                sts = 0;
            }
            createdon = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.getDefault()).format(new Date());
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userid",empid);
            jsonObject.put("reportdate",report);
            jsonObject.put("description",desc);
            jsonObject.put("status",sts);
            jsonObject.put("starttime",start);
            jsonObject.put("endtime",end);
            jsonObject.put("roleid",this.roleid);
            jsonObject.put("pgid",this.pgid);
            jsonObject.put("createdon",createdon);
            final String requestBody = jsonObject.toString();
            Log.e("body",requestBody);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Toast.makeText(getApplicationContext(),"Task Added Successfully",Toast.LENGTH_SHORT).show();
                    String date = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
                    reportDate.setText(date);
                    startTime.setText("");
                    endTime.setText("");
                    description.setText("");
                    progressLayout.setVisibility(View.GONE);
                    finish();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Volley Error", String.valueOf(error));
                    progressLayout.setVisibility(View.GONE);
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };
            requestQueue.add(stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void selectSpinnerData(Spinner status) {
        List<String> list = new ArrayList<>();
        list.add("Select Status");
        list.add("Completed");
        list.add("Pending");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        status.setAdapter(arrayAdapter);
    }
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
    private void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        Objects.requireNonNull(inputMethodManager).hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
