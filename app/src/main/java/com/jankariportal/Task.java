package com.jankariportal;

public class Task {
    String date,summary;
    int taskCount;

    public Task(String date, String summary, int taskCount) {
        this.date = date;
        this.summary = summary;
        this.taskCount = taskCount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public int getTaskCount() {
        return taskCount;
    }

    public void setTaskCount(int taskCount) {
        this.taskCount = taskCount;
    }
}
