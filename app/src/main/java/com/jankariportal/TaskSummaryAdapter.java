package com.jankariportal;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class TaskSummaryAdapter extends RecyclerView.Adapter<TaskSummaryAdapter.ViewHolder> {
    private List<TaskSummary> taskSummaryList = new ArrayList<>();

    public TaskSummaryAdapter(List<TaskSummary> taskSummaryList) {
        this.taskSummaryList = taskSummaryList;
    }

    @Override
    public int getItemCount() {
        return taskSummaryList.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_summary,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        TaskSummary taskSummary = taskSummaryList.get(position);
        holder.eTime.setText(taskSummary.getEndDate());
        holder.sTime.setText(taskSummary.getStartDate());
        holder.desc.setText(taskSummary.getDescription());
        holder.status.setText(taskSummary.getStatus());
        if (holder.status.getText().toString().equals("Completed")){
            holder.status.setTextColor(Color.rgb(0,128,0));
        }
        if (holder.status.getText().toString().equals("Pending")){
            holder.status.setTextColor(Color.RED);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView sTime,eTime,desc,status;
        ViewHolder(View view){
            super(view);
            sTime = view.findViewById(R.id.ts_sd);
            eTime = view.findViewById(R.id.ts_ed);
            desc = view.findViewById(R.id.ts_desc);
            status = view.findViewById(R.id.ts_status);
        }
    }
}
