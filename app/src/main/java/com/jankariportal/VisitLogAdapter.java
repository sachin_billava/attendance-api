package com.jankariportal;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.meg7.widget.CircleImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class VisitLogAdapter extends RecyclerView.Adapter<VisitLogAdapter.MyViewHolder> {

    private List<VisitLog> logList = new ArrayList<>();
    Context context;
    String visitinpic, visitoutpic;

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_visit, parent, false);
        context = parent.getContext();
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        VisitLog visitLog = logList.get(position);
        holder.cName.setText(visitLog.getClientName());
        holder.inTime.setText(visitLog.getTimeIn());
        if (visitLog.getTimeOut().equals("null")) {
            holder.outTime.setText("");
        } else {
            holder.outTime.setText(visitLog.getTimeOut());
        }
        holder.addressIn.setText(visitLog.getAddressIn());
        holder.addressOut.setText(visitLog.getAddressOut());
        String inPic = visitLog.getInImage();
        String outPic = visitLog.getOutImage();
        Picasso.get().load(context.getResources().getString(R.string.domain_url) + context.getResources().getString(R.string.get_visitinpic_path) + inPic).fit().noFade()
                .into(holder.inImage);
        Picasso.get().load(context.getResources().getString(R.string.domain_url) + context.getResources().getString(R.string.get_visitoutpic_path) + outPic).fit().noFade()
                .into(holder.outImage);
        holder.inImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImage(holder.inImage);
            }
        });
        holder.outImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImage(holder.outImage);
            }
        });
    }
    private void openImage(ImageView view){
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final View dialogLayout = LayoutInflater.from(context).inflate(R.layout.image_dialog,null);
        ImageView imageView = dialogLayout.findViewById(R.id.dailogImage);

        imageView.setImageDrawable(view.getDrawable());

        builder.setView(dialogLayout);
        builder.setCancelable(true);
        builder.show();

    }
    @Override
    public int getItemCount() {
        return logList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView cName, addressIn, addressOut, inTime, outTime;
        public CircleImageView inImage, outImage;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            cName = itemView.findViewById(R.id.client_name);
            inTime = itemView.findViewById(R.id.time_in);
            outTime = itemView.findViewById(R.id.time_out);
            inImage = itemView.findViewById(R.id.in_image);
            outImage = itemView.findViewById(R.id.out_image);
            addressIn = itemView.findViewById(R.id.visitIn);
            addressOut = itemView.findViewById(R.id.visitOut);
        }
    }

    public VisitLogAdapter(List<VisitLog> logs) {
        this.logList = logs;
    }


}
