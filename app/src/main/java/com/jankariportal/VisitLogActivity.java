package com.jankariportal;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class VisitLogActivity extends AppCompatActivity {
    private EditText dateSelector;
    private RecyclerView recyclerView;
    private Calendar calendar;
    List<VisitLog> visitLogList = new ArrayList<>();
    VisitLogAdapter adapter = new VisitLogAdapter(visitLogList);
    String empid, date;
    View progressLayout;
    Context context;
    String name, inTime, outTime, inImage, outImage, longiIn, latiIn, longiOut, latiOut;
    String add1, add2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit_log);
        setTitle("Visit Log");
        context = this;
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        final Context context = getApplicationContext();
        dateSelector = findViewById(R.id.dateSelector);
        calendar = Calendar.getInstance();
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        progressLayout = findViewById(R.id.progress_layout);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        SharedPreferences sharedPreferences = getSharedPreferences("Jankari", MODE_PRIVATE);
        empid = sharedPreferences.getString("userid", "");
        final DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateEditTextDate(dateSelector);
            }
        };
        dateSelector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(VisitLogActivity.this, dateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        date = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        dateSelector.setText(date);
        showLogs();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void updateEditTextDate(EditText editText) {
//        editText.setText(date = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date()));
        visitLogList.clear();
        date = null;
        String format = "dd-MM-yyyy";
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        date = simpleDateFormat.format(calendar.getTime());
        editText.setText(date);
        showLogs();
    }

    private void showLogs() {
        recyclerView.setVisibility(View.VISIBLE);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.divider));
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(adapter);
        progressLayout.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                GetValuesFromJSON getValuesFromJSON = new GetValuesFromJSON();
                getValuesFromJSON.execute();
            }
        },500);
    }

    public String prepareAddress(double longti, double lati) {
        String data = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lati, longti, 1);
            data = addresses.get(0).getAddressLine(0);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }

    private class GetValuesFromJSON extends AsyncTask<Void, Void, Void> {
        boolean norecord = false;
        @Override
        protected Void doInBackground(Void... voids) {
            JSONHandler jsonHandler = new JSONHandler();

            String format = "yyyy-MM-dd";
            @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
            date = simpleDateFormat.format(calendar.getTime());

            String jsonStr = jsonHandler.makeJSONCall("http://api.jankari.org.in/api/Employee/GetEmployeeVisitLogList?" +
                    "whr= where EmployeeVisitLog.empid = '" + empid + "' and Convert(date,EmployeeVisitLog.date)=convert(date,'" + date + "',111)");
            if(jsonStr.equalsIgnoreCase("[]\n")){
                norecord = true;
            }else if (jsonStr != null) {
                try {
                    JSONArray jsonArray = new JSONArray(jsonStr);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        name = jsonObject.getString("empname");
                        inTime = jsonObject.getString("visitintime");
                        outTime = jsonObject.getString("visitouttime");
                        inImage = jsonObject.getString("visitinphoto");
                        outImage = jsonObject.getString("visitoutphoto");
                        longiIn = jsonObject.getString("startloclongitude");
                        latiIn = jsonObject.getString("startloclatitude");
                        longiOut = jsonObject.getString("endloclongitude");
                        latiOut = jsonObject.getString("endloclatitude");
                        if(outTime == "null"){//need to be removed
                            outTime = "";
                        }
                        if (longiIn == "null" && latiIn == "null") {

                        } else {
                            add1 = prepareAddress(Double.parseDouble(longiIn), Double.parseDouble(latiIn));
                        }
                        if (longiOut == "null" && latiOut == "null") {

                        } else {
                            add2 = prepareAddress(Double.parseDouble(longiOut), Double.parseDouble(latiOut));
                        }//till here
                        VisitLog visitLog = new VisitLog(name, add1, add2, inTime, outTime, inImage, outImage);
                        visitLogList.add(visitLog);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            adapter.notifyDataSetChanged();
            progressLayout.setVisibility(View.GONE);
            if (norecord){
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("No record found...");
                builder.setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.setTitle("Sorry!!!");
                alert.show();
            }
        }
    }
}
