package com.jankariportal;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    Button loginBtn;
    EditText username, password;
    //    private static final int LOCATION_CODE = 100;
//    private static final int CAMERA_CODE = 101;
//    private static final int INTERNET_CODE = 102;
    private static final int req = 100;
    //    LottieAnimationView imageView;
    CheckBox rememberme;
    private SharedPreferences mPrefs;
    static final String PREFS_NAME = "Jankari";
    String previousActivity = null;
    LinearLayout inputs;
    private static LoginActivity instance;
    private View progress;
    String user, pass;
    private String urlLogin = "http://api.jankari.org.in/api/JankariPortal/Login";
    String jsonResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.frombottom);
        instance = this;
        if (!isOnline(LoginActivity.this)) buildDialog(LoginActivity.this).show();
        inputs = findViewById(R.id.inputs);
        inputs.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fromtop));
        loginBtn = findViewById(R.id.loginBtn);
        progress = findViewById(R.id.progress_layout);
        username = findViewById(R.id.etUserName);
        password = findViewById(R.id.etPassword);
//        imageView = findViewById(R.id.imageView);
//        animate(imageView);
        rememberme = findViewById(R.id.cbRemember);

        mPrefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        Intent mIntent = getIntent();
        previousActivity = mIntent.getStringExtra("FROM_ACTIVITY");
        username.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
        password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
        loginBtn.startAnimation(animation);

        if (CheckingPermissionIsEnabledOrNot()) {
//            Toast.makeText(LoginActivity.this, "All Permissions Granted Successfully", Toast.LENGTH_LONG).show();
        } else {

            requestAllPermissions();

        }
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progress.setVisibility(View.VISIBLE);
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                user = username.getText().toString().trim();
                pass = password.getText().toString().trim();

                if (user.isEmpty()) {
                    username.setError("Please enter UserName");
                }
                if (pass.isEmpty()) {
                    password.setError("Please enter Password");
                }

                userLogin();

            }
        });


    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        assert inputMethodManager != null;
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void userLogin() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, urlLogin, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //show dialog

                try {
                    JSONArray jsonarray = new JSONArray(response);

                    for (int i = 0; i < jsonarray.length(); i++) {
                        JSONObject jsonObject = jsonarray.getJSONObject(i);
                        if (jsonObject.getString("Result").equalsIgnoreCase("success")) {
                            String userid = jsonObject.getString("UserId");
                            String username = jsonObject.getString("UserName");
                            String userrole = jsonObject.getString("Userrole");
                            String fullname = jsonObject.getString("FullName");

                            //SP
                            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("userid", userid);
                            editor.putString("username", username);
                            editor.putString("userrole", userrole);
                            editor.putString("pass", pass);
                            editor.putString("fullname", fullname);
                            editor.apply();

                            if (rememberme.isChecked()) {
                                Boolean boolisChecked = rememberme.isChecked();
                                SharedPreferences.Editor editor1 = mPrefs.edit();
                                editor1.putString("pref_name", username);
                                editor1.putString("pref_pass", password.getText().toString());
                                editor1.putBoolean("pref_check", boolisChecked);
                                editor1.commit();
                            } else {
                                mPrefs.edit().clear().apply();
                            }

                            progress.setVisibility(View.GONE);
                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            finish();

                        } else if (jsonObject.getString("Result").equalsIgnoreCase("FALIURE")) {
                            progress.setVisibility(View.GONE);
                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                            Toast.makeText(LoginActivity.this, "Check login credentials!", Toast.LENGTH_SHORT).show();
                        } else if (jsonObject.getString("Result").equalsIgnoreCase("inactive")) {
                            progress.setVisibility(View.GONE);
                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                            Toast.makeText(LoginActivity.this, "User is inactive", Toast.LENGTH_SHORT).show();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", user);
                params.put("password", pass);
                return params;
            }
        };

        requestQueue.add(stringRequest);
    }

    private boolean check(String in) {
        int count = 0;
        for (int i = 0; i < in.length(); i++) {
            if (Character.isUpperCase(in.charAt(i))) {
                count++;
            }
        }
        if (count == 2) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else return false;
        } else
            return false;
    }

    private void requestAllPermissions() {
        String perms[] = {"Manifest.permission.CAMERA", "Manifest.permission.ACCESS_FINE_LOCATION", "android.Manifest.permission.WRITE_EXTERNAL_STORAGE", "android.Manifest.permission.READ_EXTERNAL_STORAGE"};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{
                    Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            }, req);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case req:
                if (grantResults.length > 0) {
                    boolean camPer = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean locPer = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean writePer = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean readPer = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                    if (camPer && locPer && writePer && readPer) {
//                        Toast.makeText(LoginActivity.this, "Permission Granted", Toast.LENGTH_LONG).show();
                    } else {

//                        Toast.makeText(LoginActivity.this, "Permission Denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    public boolean CheckingPermissionIsEnabledOrNot() {
        int FirstPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), "Manifest.permission.CAMERA");
        int SecondPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), "Manifest.permission.ACCESS_FINE_LOCATION");
        int ThirdPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), "Manifest.permission.WRITE_EXTERNAL_STORAGE");
        int ForthPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), "Manifest.permission.READ_EXTERNAL_STORAGE");

        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED &&
                SecondPermissionResult == PackageManager.PERMISSION_GRANTED &&
                ThirdPermissionResult == PackageManager.PERMISSION_GRANTED &&
                ForthPermissionResult == PackageManager.PERMISSION_GRANTED;
    }

    public AlertDialog.Builder buildDialog(Context c) {

        AlertDialog.Builder builder = new AlertDialog.Builder(c).setCancelable(false);
        builder.setTitle("No Internet Connection");
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setMessage("Turn on Mobile Data or Wifi to proceed.");

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setCancelable(false);

        return builder;
    }
}
