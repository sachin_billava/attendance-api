package com.jankariportal.ui.visit;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jankariportal.AppLocationService;
import com.jankariportal.AttendanceLog;
import com.jankariportal.JSONHandler;
import com.jankariportal.LocationAddress;
import com.jankariportal.R;
import com.jankariportal.ShowSettingsAlert;
import com.jankariportal.VisitLogActivity;
import com.jankariportal.ui.attendance.AttendanceFragment;
import com.jankariportal.ui.visit.VisitFragment;
import com.meg7.widget.CircleImageView;
import com.squareup.picasso.Picasso;

import org.jibble.simpleftp.SimpleFTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_CANCELED;
import static android.content.Context.LOCATION_SERVICE;
import static com.jankariportal.custom.NetworkDetails.isNetworkAvailable;
import static com.jankariportal.ui.profile.ProfileFragment.PERMISSIONS_EXTERNAL_STORAGE;
import static com.jankariportal.ui.profile.ProfileFragment.REQUEST_EXTERNAL_PERMISSION_CODE;

public class VisitFragment extends Fragment implements LocationListener {

    private Spinner dpdVisitType;
    private TextView address, userName, clickToRefresh;
    private CheckBox checkBox;
    int mintime = 10000;
    int minDist = 100;
    private Button visitIn, visitOut;
    private CircleImageView image;
    private int REQUEST_CAMERA = 2;
    int requestCode = 101;
    boolean flag;
    Calendar calendar;
    ImageView refreshIcon;
    LocationManager locationManager;
    LocationListener locationListener;
    AppLocationService appLocationService;
    boolean lessThanNoug = false;
    View root;
    private FloatingActionButton fab;
    private String empId = null, date = null, empname = null, visit_in_pic, visit_out_pic, profilePic, endtime;
    File destination;
    double latitude = 0;
    double longitude = 0;
    String urlVisitIn = null, urlVisitOut = null;
    String locationAddress;
    String isFinal, myString, attres, dateSP, attendanceParamIn, attendanceParamOut;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        locationListener = this;
        root = inflater.inflate(R.layout.fragment_visit, container, false);
        dpdVisitType = root.findViewById(R.id.spinner);
        address = root.findViewById(R.id.address);
        checkBox = root.findViewById(R.id.outCheck);
        checkBox.setVisibility(View.GONE);
        clickToRefresh = root.findViewById(R.id.clickToRefresh);
        refreshIcon = root.findViewById(R.id.refresh);
        visitIn = root.findViewById(R.id.visitIn);
        fab = root.findViewById(R.id.floatingActionButton);
        appLocationService = new AppLocationService(getActivity());
        image = root.findViewById(R.id.profilePicture);
        userName = root.findViewById(R.id.tvAttendanceWelcome);
        visitOut = root.findViewById(R.id.visitOut);
        visitOut.setVisibility(View.GONE);
        calendar = Calendar.getInstance();
        flag = true;
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.N){
            lessThanNoug = true;
        } else{
            lessThanNoug = false;
        }
        locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[] {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},requestCode);
        }
        if(lessThanNoug){
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, mintime, minDist, this);
        }else{
            //fetchaddress
            fetchAddress();



        }
        //get current time
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        date = sdf.format(now);

        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences("Jankari", Context.MODE_PRIVATE);
        empId = sharedPreferences.getString("userid", "");
        empname = sharedPreferences.getString("fullname", "");
        profilePic = sharedPreferences.getString("empphoto", "");

        SharedPreferences sharedPreferences1 = this.getActivity().getSharedPreferences("checkFinal", Context.MODE_PRIVATE);
//        myString = getActivity().getIntent().getStringExtra("isFinal");
        dateSP = sharedPreferences1.getString("dateSP", "");
        if (dateSP.equals(date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date()))) {
            myString = sharedPreferences1.getString("isFinal", "");
        } else {
            sharedPreferences1.edit().clear();
            myString = "No";
        }

        if (myString.equalsIgnoreCase("null") || myString.equalsIgnoreCase(null)
                || myString.equalsIgnoreCase("No") || myString.equalsIgnoreCase("")) {
            myString = "No";
        } else {
            myString = "Yes";
        }

        checkVisit();
        new checkAttendance().execute();
        address.setVisibility(View.INVISIBLE);
        image.setVisibility(View.INVISIBLE);

//        fetchAddress();
        selectSpinnerData(dpdVisitType);
        visitIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                captureImageFromCamera(v);
            }
        });
        visitOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                captureImageFromCamera(v);
            }
        });

        if (profilePic.equals(null) || profilePic.equals("null")) {
            image.setVisibility(View.VISIBLE);
        } else {
            image.setVisibility(View.VISIBLE);
            Picasso.get().load(getResources().getString(R.string.domain_url) + getResources().getString(R.string.get_profilepic_path) + profilePic)
                    .fit()
                    .centerCrop(Gravity.CENTER)
                    .noFade()
                    .into(image);
        }

        urlVisitIn = "http://api.jankari.org.in/api/Employee/AddEMPVisitLog/" + empId;

        clickToRefresh.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View v) {
                Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate);
                refreshIcon.startAnimation(animation);
//                fetchAddress();
                if (lessThanNoug){
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
                }else{
                    fetchAddress();

                }
            }
        });
        refreshIcon.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View v) {
                Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate);
                refreshIcon.startAnimation(animation);
//                fetchAddress();
                if (lessThanNoug){
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
                }else{
                    fetchAddress();
                }
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), VisitLogActivity.class);
                startActivity(intent);
            }
        });
        return root;
    }

    private void selectSpinnerData(Spinner dpdVisitType) {
        List<String> list = new ArrayList<>();
        list.add("Select Visit Type");
        list.add("School Visit");
        list.add("Office Visit");
        list.add("Workshop Visit");
        list.add("Survey Visit");
        ArrayAdapter arrayAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, list);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dpdVisitType.setAdapter(arrayAdapter);
    }


    private void captureImageFromCamera(View v) {
        if (v == visitIn) {
            flag = true;
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, REQUEST_CAMERA);
        }
        if (v == visitOut) {
            flag = false;
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, REQUEST_CAMERA);
        }
    }

    @SuppressLint("MissingPermission")
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("Code", String.valueOf(requestCode));
        if (resultCode == RESULT_CANCELED) {
//            fetchAddress();
            if (lessThanNoug){
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            }else{
                fetchAddress();

            }
            return;
        }
        if (requestCode == REQUEST_CAMERA) {
//            fetchAddress();
            if (lessThanNoug){
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            }else{
                fetchAddress();

            }
            if (flag) {
                onCaptureImageResult(data);
                visitIn.setVisibility(View.GONE);
                dpdVisitType.setVisibility(View.GONE);
                visitOut.setVisibility(View.VISIBLE);
                checkBox.setVisibility(View.VISIBLE);
                //addVisitIn();
                if (checkExternalStoragePermission(getActivity())) {
                    // Continue with your action after permission request succeed
                    VisitFragment.UploadVisitPic async = new VisitFragment.UploadVisitPic();

                    if (isNetworkAvailable(getActivity())) {
                        async.execute(destination.toString());
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.no_working_internet_msg), Toast.LENGTH_LONG).show();
                    }
                }
            } else {
                try {
                    onCaptureImageResult1(data);
                    visitIn.setVisibility(View.VISIBLE);
                    dpdVisitType.setVisibility(View.VISIBLE);
                    visitOut.setVisibility(View.GONE);
                    checkBox.setVisibility(View.GONE);
//                    addVisitOut();
                    if (checkExternalStoragePermission(getActivity())) {
                        VisitFragment.UploadVisitOutPic async = new VisitFragment.UploadVisitOutPic();

                        if (isNetworkAvailable(getActivity())) {
                            async.execute(destination.toString());
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.no_working_internet_msg), Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 60, bytes);

        visit_in_pic = empId + "_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime()) + ".jpg";

        destination = new File(Environment.getExternalStorageDirectory(), visit_in_pic);

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
//            destination.delete();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void onCaptureImageResult1(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 60, bytes);

        visit_out_pic = empId + "_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime()) + ".jpg";

        destination = new File(Environment.getExternalStorageDirectory(), visit_out_pic);

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
//            destination.delete();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void fetchAddress() {

                Location location = appLocationService.getLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    LocationAddress locationAddress = new LocationAddress();
                    locationAddress.getAddressFromLocation(latitude, longitude, getActivity(), new GeocoderHandler());
                } else {
                    new ShowSettingsAlert(getContext());
                }

    }

    public boolean checkExternalStoragePermission(Activity activity) {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            return true;
        }

        int readStoragePermissionState = ContextCompat.checkSelfPermission(activity, READ_EXTERNAL_STORAGE);
        int writeStoragePermissionState = ContextCompat.checkSelfPermission(activity, WRITE_EXTERNAL_STORAGE);
        boolean externalStoragePermissionGranted = readStoragePermissionState == PackageManager.PERMISSION_GRANTED &&
                writeStoragePermissionState == PackageManager.PERMISSION_GRANTED;
        if (!externalStoragePermissionGranted) {
            requestPermissions(PERMISSIONS_EXTERNAL_STORAGE, REQUEST_EXTERNAL_PERMISSION_CODE);
        }

        return externalStoragePermissionGranted;
    }

    @Override
    public void onLocationChanged(Location location) {
        longitude = location.getLongitude();
        latitude = location.getLatitude();
        LocationAddress locationAddress = new LocationAddress();
        locationAddress.getAddressFromLocation(latitude,longitude,getActivity(),new GeocoderHandler());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        new ShowSettingsAlert(getContext());
    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }
            address.setText(locationAddress);
        }
    }

    class UploadVisitPic extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            // ftpClient=uploadingFilestoFtp();
            try {
                SimpleFTP ftp = new SimpleFTP();

                ftp.connect(getResources().getString(R.string.ftp_host_name), Integer.parseInt(getResources().getString(R.string.ftp_port)), getResources().getString(R.string.ftp_username), getResources().getString(R.string.ftp_password));

                ftp.bin();

                // Change to a new working directory on the FTP server.
                ftp.cwd(getResources().getString(R.string.post_visitinpic_path));

                File extStore = Environment.getExternalStorageDirectory();
                String mPath = extStore.getAbsolutePath();

                attendanceParamIn = params[0];

                // Upload some files.
                ftp.stor(new File(params[0]));


                ftp.disconnect();

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            if (isNetworkAvailable(getActivity())) {
                if (attres.equalsIgnoreCase("\"Failure\"")) {
                    new UploadAttendancePic().execute();
                }
                addVisitIn();
            } else {
                Toast.makeText(getActivity(), getString(R.string.no_working_internet_msg), Toast.LENGTH_LONG).show();
            }
        }
    }

    class UploadAttendancePic extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            // ftpClient=uploadingFilestoFtp();
            try {
                SimpleFTP ftp = new SimpleFTP();

                ftp.connect(getResources().getString(R.string.ftp_host_name), Integer.parseInt(getResources().getString(R.string.ftp_port)), getResources().getString(R.string.ftp_username), getResources().getString(R.string.ftp_password));

                ftp.bin();

                // Change to a new working directory on the FTP server.
                ftp.cwd(getResources().getString(R.string.post_attendanceinpic_path));

                File extStore = Environment.getExternalStorageDirectory();
                String mPath = extStore.getAbsolutePath();
                // Upload some files.
                ftp.stor(new File(attendanceParamIn));

                ftp.disconnect();

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            if (isNetworkAvailable(getActivity())) {
//                addVisitIn();
            } else {
                Toast.makeText(getActivity(), getString(R.string.no_working_internet_msg), Toast.LENGTH_LONG).show();
            }
        }
    }

    class UploadVisitOutPic extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            // ftpClient=uploadingFilestoFtp();
            try {
                SimpleFTP ftp = new SimpleFTP();

                ftp.connect(getResources().getString(R.string.ftp_host_name), Integer.parseInt(getResources().getString(R.string.ftp_port)), getResources().getString(R.string.ftp_username), getResources().getString(R.string.ftp_password));

                ftp.bin();

                // Change to a new working directory on the FTP server.
                ftp.cwd(getResources().getString(R.string.post_visitoutpic_path));

                File extStore = Environment.getExternalStorageDirectory();
                String mPath = extStore.getAbsolutePath();

                attendanceParamOut = params[0];

                // Upload some files.
                ftp.stor(new File(params[0]));

                ftp.disconnect();

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            attres = null;
            try {
                new checkAttendance().execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPostExecute(String result) {

            if (isNetworkAvailable(getActivity())) {

                if (checkBox.isChecked()) {
                    isFinal = "Yes";
                } else {
                    isFinal = "No";
                }
                if (attres.equalsIgnoreCase("\"Markedin\"") && isFinal.equalsIgnoreCase("Yes")) {
                    new UploadAttendanceOutPic().execute();
                }
                addVisitOut();
            } else {
                Toast.makeText(getActivity(), getString(R.string.no_working_internet_msg), Toast.LENGTH_LONG).show();
            }
        }
    }

    class UploadAttendanceOutPic extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            // ftpClient=uploadingFilestoFtp();
            try {
                SimpleFTP ftp = new SimpleFTP();

                ftp.connect(getResources().getString(R.string.ftp_host_name), Integer.parseInt(getResources().getString(R.string.ftp_port)), getResources().getString(R.string.ftp_username), getResources().getString(R.string.ftp_password));

                ftp.bin();

                // Change to a new working directory on the FTP server.
                ftp.cwd(getResources().getString(R.string.post_attendanceoutpic_path));

                File extStore = Environment.getExternalStorageDirectory();
                String mPath = extStore.getAbsolutePath();
                // Upload some files.
                ftp.stor(new File(attendanceParamOut));

                ftp.disconnect();

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {

            if (isNetworkAvailable(getActivity())) {
//                addVisitOut();
            } else {
                Toast.makeText(getActivity(), getString(R.string.no_working_internet_msg), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void checkVisit() {
        final String checkdate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

        RequestQueue queue = Volley.newRequestQueue(getContext());
        String url = "http://api.jankari.org.in/api/Employee/CheckVisitlog/" + checkdate + "/" + empId + "/" + myString;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    if (response.equalsIgnoreCase("\"Failure\"") || response.equals("\"\"")) {
                        visitIn.setVisibility(View.VISIBLE);
                        visitOut.setVisibility(View.GONE);
                        dpdVisitType.setVisibility(View.VISIBLE);
                        address.setVisibility(View.VISIBLE);
                        refreshIcon.setVisibility(View.VISIBLE);
                        clickToRefresh.setVisibility(View.VISIBLE);
                    } else if (response.equalsIgnoreCase("\"Markedin\"")) {
                        visitIn.setVisibility(View.GONE);
                        checkBox.setVisibility(View.VISIBLE);
                        visitOut.setVisibility(View.VISIBLE);
                        dpdVisitType.setVisibility(View.GONE);
                        address.setVisibility(View.VISIBLE);
                        refreshIcon.setVisibility(View.VISIBLE);
                        clickToRefresh.setVisibility(View.VISIBLE);
                    } else if (response.equalsIgnoreCase("\"Markedout\"")) {
                        visitIn.setVisibility(View.VISIBLE);
                        visitOut.setVisibility(View.GONE);
                        dpdVisitType.setVisibility(View.VISIBLE);
                        checkBox.setVisibility(View.GONE);
                        address.setVisibility(View.VISIBLE);
                        refreshIcon.setVisibility(View.VISIBLE);
                        clickToRefresh.setVisibility(View.VISIBLE);
                    } else {
                        visitIn.setVisibility(View.GONE);
                        visitOut.setVisibility(View.GONE);
                        dpdVisitType.setVisibility(View.GONE);
                        checkBox.setVisibility(View.GONE);
                        address.setVisibility(View.VISIBLE);
                        address.setText("Final Visit Marked.");
                        refreshIcon.setVisibility(View.GONE);
                        clickToRefresh.setVisibility(View.GONE);

                        Intent intent = new Intent(getContext(), AttendanceFragment.class);
                        intent.putExtra("isFinal", response);
                        startActivity(intent);
                    }
                    image.setVisibility(View.VISIBLE);
                    userName.setText(empname);
                } catch (
                        Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(stringRequest);
    }

    private void addVisitIn() {
        try {
            endtime = null;
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("empid", empId);
            jsonObject.put("date", date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date()));
            jsonObject.put("visittype", dpdVisitType.getSelectedItem().toString());
            jsonObject.put("visitintime", endtime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date()));
            jsonObject.put("startloclongitude", longitude);
            jsonObject.put("startloclatitude", latitude);
            jsonObject.put("locationname", locationAddress);
            jsonObject.put("visitinphoto", visit_in_pic);
            jsonObject.put("createby", empId);
            jsonObject.put("createon", date);

            final String requestBody = jsonObject.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, urlVisitIn, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("VOLLEY", response);
                    Toast.makeText(getActivity(), "Visit Marked Successfully", Toast.LENGTH_SHORT).show();
                    destination.delete();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
//                    dialog.dismiss();
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addVisitOut() {
        if (checkBox.isChecked()) {
            isFinal = "Yes";
            visitIn.setVisibility(View.GONE);
        } else {
            isFinal = "No";
        }
        urlVisitOut = "http://api.jankari.org.in/api/Employee/UpdateEMPVisitLog/" + empId + "/" + isFinal;

        try {
            endtime = null;
//            date = null;
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("empid", empId);
            jsonObject.put("date", date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date()));
//            jsonObject.put("visittype", dpdVisitType.getSelectedItem().toString());
            jsonObject.put("visitouttime", endtime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date()));
            jsonObject.put("endloclongitude", longitude);
            jsonObject.put("endloclatitude", latitude);
//            jsonObject.put("locationname", locationAddress);
            jsonObject.put("visitoutphoto", visit_out_pic);
            jsonObject.put("updateby", empId);
            jsonObject.put("updateon", date);

            SharedPreferences sharedPreferences = getActivity().getSharedPreferences("checkFinal", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("isFinal", isFinal);
            editor.putString("dateSP", date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date()));
            editor.commit();

            final String requestBody = jsonObject.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, urlVisitOut, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("VOLLEY", response);
                    checkVisit();
                    Toast.makeText(getActivity(), "Visit Marked Successfully", Toast.LENGTH_SHORT).show();
                    destination.delete();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
//                    dialog.dismiss();
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class checkAttendance extends AsyncTask<Void, Void, Void> {
        JSONObject jsonObject;
        String replace;
        String checkdate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        String url = "http://api.jankari.org.in/api/Employee/CheckAttlog/" + checkdate + "/" + empId;

        @Override
        protected Void doInBackground(Void... voids) {
            JSONHandler jsonHandler = new JSONHandler();
            String jsonStr = jsonHandler.makeJSONCall(url);
            if (jsonStr != null) {
                try {
                    replace = jsonStr;
                    attres = replace.replace("\n", "");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getActivity().getApplicationContext(), "PROBLEM!!!", Toast.LENGTH_SHORT).show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            super.onPostExecute(aVoid);
//            adapter.notifyDataSetChanged();
            image.setVisibility(View.VISIBLE);
            userName.setText(empname);

        }
    }
}
