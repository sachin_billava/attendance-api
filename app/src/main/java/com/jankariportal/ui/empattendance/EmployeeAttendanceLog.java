package com.jankariportal.ui.empattendance;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jankariportal.AttendanceLog;
import com.jankariportal.JSONHandler;
import com.jankariportal.LogAdapter;
import com.jankariportal.R;
import com.jankariportal.VisitLogActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class EmployeeAttendanceLog extends Fragment {

    View root;
    private RecyclerView recyclerView;
    private List<AttendanceLog> logList = new ArrayList<>();
    private LogAdapter adapter;
    private String empid;
    View progressLayout;
    private Calendar calendar;
    String name,date, timein, address1, address2, timeout, inimg, outimg;
    String slongitude, slatitude, elongitude, elatitude;
    EditText selectDate;
    DatePickerDialog.OnDateSetListener dateSetListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        root = inflater.inflate(R.layout.fragment_employee_attendancelog, container, false);
        recyclerView = root.findViewById(R.id.recycler_view);
        adapter = new LogAdapter(logList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity().getApplicationContext(), DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.divider));
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(adapter);
        progressLayout = root.findViewById(R.id.progress_layout);
        selectDate = root.findViewById(R.id.selectDate);
        calendar = Calendar.getInstance();
        String cdate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        selectDate.setText(cdate);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("Jankari", MODE_PRIVATE);
        empid = sharedPreferences.getString("userid", "");

       dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateEditTextDate(selectDate);
            }
        };

        selectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getContext(), dateSetListener,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        progressLayout.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                GetValuesFromJSON getValuesFromJSON = new GetValuesFromJSON();
                getValuesFromJSON.execute();
            }
        },500);
        return root;
    }

    private void updateEditTextDate(EditText editText) {
        logList.clear();
        date = null;
        String format = "yyyy-MM-dd";
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        date = simpleDateFormat.format(calendar.getTime());
        editText.setText(date);

        progressLayout.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                GetValuesFromJSON getValuesFromJSON = new GetValuesFromJSON();
                getValuesFromJSON.execute();
            }
        },500);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.leave);
        if (item != null)
            item.setVisible(false);
    }
    private class GetValuesFromJSON extends AsyncTask<Void, Void, Void> {
        JSONObject jsonObject;
        boolean norecord = false;
        String urlEmpAtt = "http://api.jankari.org.in/api/Employee/GetAttendanceLogList?whr=where EmployeeProgram.reportto='"+empid+"' and AttendanceLog.date='"+selectDate.getText().toString()+"'";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            JSONHandler jsonHandler = new JSONHandler();
            String jsonStr = jsonHandler.makeJSONCall(urlEmpAtt);
            if(jsonStr.equalsIgnoreCase("[]\n")){
                norecord = true;
            }else if (jsonStr != null) {
                try {
                    JSONArray jsonArray = new JSONArray(jsonStr);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);
                        name = jsonObject.getString("empname");
                        date = jsonObject.getString("date").substring(0, 10);
                        timein = jsonObject.getString("starttime");
                        timeout = jsonObject.getString("endtime");
                        slongitude = jsonObject.getString("startloclongitude");
                        slatitude = jsonObject.getString("startloclatitude");
                        elongitude = jsonObject.getString("endloclongitude");
                        elatitude = jsonObject.getString("endloclatitude");
                        inimg = jsonObject.getString("photoin");
                        outimg = jsonObject.getString("photoout");
                        if (slatitude == "null" && slongitude == "null") {

                        } else {
                            address1 = prepareAddress(Double.parseDouble(slongitude), Double.parseDouble(slatitude));
                        }
                        if (elatitude == "null" && elongitude == "null") {

                        } else {
                            address2 = prepareAddress(Double.parseDouble(elongitude), Double.parseDouble(elatitude));
                        }
                        AttendanceLog attendanceLog = new AttendanceLog(name,date, address1, address2, inimg, outimg, timein, timeout);
                        logList.add(attendanceLog);

                        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("Jankari", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("date", date);
                        editor.putString("intime", timein);
                        editor.putString("outtime", timeout);
                        editor.commit();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getActivity().getApplicationContext(), "PROBLEM!!!", Toast.LENGTH_SHORT).show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            adapter.notifyDataSetChanged();
            progressLayout.setVisibility(View.GONE);
            if (norecord){
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("No record found...");
                builder.setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.setTitle("Sorry!!!");
                alert.show();
            }


        }
    }
    public String prepareAddress(double longti, double lati) {
        String data = "";
        Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lati, longti, 1);
            data = addresses.get(0).getAddressLine(0);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }

}
