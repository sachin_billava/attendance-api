package com.jankariportal.ui.leaves;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.jankariportal.JSONHandler;
import com.jankariportal.LeavesNotice;
import com.jankariportal.R;
import com.jankariportal.VisitLogActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;


/**
 * A simple {@link Fragment} subclass.
 */
public class LeavesFragment extends Fragment {

    EditText startDate, endDate, leaveMessage;
    Spinner empSpinner, spinner;
    CheckBox showMoreChb;
    Button submit;
    String empid;
    String emp;
    SharedPreferences sharedPreferences;
    List<String> empNames;
    Calendar calendar;
    List<String> leaveTypes;
    Menu menu;
    HashMap<String, String> map;

    public LeavesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        View root = inflater.inflate(R.layout.fragment_leaves, container, false);
        empSpinner = root.findViewById(R.id.empName);
        startDate = root.findViewById(R.id.startDate);
        endDate = root.findViewById(R.id.endDate);
        leaveMessage = root.findViewById(R.id.leaveMessage);
        spinner = root.findViewById(R.id.leaveType);
        submit = root.findViewById(R.id.submitLeave);
        showMoreChb = root.findViewById(R.id.showMoreChb);
        empNames = new ArrayList<>();
        calendar = Calendar.getInstance();
        leaveTypes = new ArrayList<>();
        map = new HashMap<>();
        showMoreChb.setChecked(false);

        sharedPreferences = getActivity().getSharedPreferences("Jankari", Context.MODE_PRIVATE);
        emp = sharedPreferences.getString("username", "");
        empid = sharedPreferences.getString("userid", "");
        showMoreChb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    empNames.clear();
                    map.clear();
                    new GetDropDown(empNames).execute();
                } else {
                    empNames.clear();
                    map.clear();
                    map.put(empid, emp);
                    empNames.add(emp);
                    ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, empNames);
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    empSpinner.setAdapter(arrayAdapter);
                }
            }
        });

        if (!showMoreChb.isChecked()) {
            sharedPreferences = getActivity().getSharedPreferences("Jankari", Context.MODE_PRIVATE);
            emp = sharedPreferences.getString("username", "");
            empNames.clear();
            empNames.add(emp);
            ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, empNames);
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            empSpinner.setAdapter(arrayAdapter);
        }
        leaveTypes.add("Personal Leave");
        leaveTypes.add("Maternity Leave");
        leaveTypes.add("Medical Leave");
        ArrayAdapter leaveAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, leaveTypes);
        leaveAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(leaveAdapter);
//        String date = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
//        startDate.setText(date);
        final DatePickerDialog.OnDateSetListener startDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String format = "yyyy-MM-dd";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
                String date = simpleDateFormat.format(calendar.getTime());
                startDate.setText(date);
            }

        };
        final DatePickerDialog.OnDateSetListener endDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String format = "yyyy-MM-dd";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
                String date = simpleDateFormat.format(calendar.getTime());
                endDate.setText(date);
            }

        };
        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity(), startDateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });
        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity(), endDateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                composeLeaveAndMail();
            }
        });
        leaveMessage.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
        return root;
    }

    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        this.menu = menu;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.leave:
                Intent i = new Intent(getActivity(), LeavesNotice.class);
                startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    private boolean checkFields() {
        if (startDate.getText().toString().length() != 0) {
            if (endDate.getText().toString().length() != 0) {
                if (leaveMessage.getText().toString().length() != 0) {
                    return true;
                } else {
                    leaveMessage.setError("Please Enter Message");
                    return false;
                }
            } else {
                endDate.setError("Please Select Date");
                return false;
            }
        } else {
            startDate.setError("Please Select Date");
            return false;
        }

    }

    private void composeLeaveAndMail() {
        if (checkFields()) {
            //todo

            try {
                buildJson();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void buildJson() {
        final JSONObject jsonObject = new JSONObject();
        String id = null,from,to,desc,createdBy;
        int tid = 0;
        if (spinner.getSelectedItemPosition() == 0){
            tid = 1;
        }
        if (spinner.getSelectedItemPosition() == 1){
            tid = 2;
        }
        if (spinner.getSelectedItemPosition() == 2){
            tid = 3;
        }
        if (showMoreChb.isChecked()){
            for (Map.Entry entry : map.entrySet()){
                if (empSpinner.getSelectedItem().equals(entry.getValue())){
                    id = (String) entry.getKey();
                    break;
                }
            }
        }else{
            id = empid;
        }
        from = startDate.getText().toString();
        to = endDate.getText().toString();
        desc = leaveMessage.getText().toString();
        createdBy = empid;

        try {
            jsonObject.put("empid",id);
            jsonObject.put("leavetid",tid);
            jsonObject.put("leavefrom",from);
            jsonObject.put("leaveto",to);
            jsonObject.put("leavedescription",desc);
            jsonObject.put("createby",createdBy);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //todo call send data
        String url = "http://api.jankari.org.in/api/Employee/AddEmployeeLeave";
        final RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.equalsIgnoreCase("\"success\"")){
                    Toast.makeText(getContext(), "Success", Toast.LENGTH_SHORT).show();
                }
                if (response.equalsIgnoreCase("\"already\"")){
                    Toast.makeText(getContext(), "Already", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
          protected Map<String,String> getParams(){
              String id = null,from,to,desc,createdBy;
              int tid = 0;
              if (spinner.getSelectedItemPosition() == 0){
                  tid = 1;
              }
              if (spinner.getSelectedItemPosition() == 1){
                  tid = 2;
              }
              if (spinner.getSelectedItemPosition() == 2){
                  tid = 3;
              }
              if (showMoreChb.isChecked()){
                  for (Map.Entry entry : map.entrySet()){
                      if (empSpinner.getSelectedItem().equals(entry.getValue())){
                          id = (String) entry.getKey();
                          break;
                      }
                  }
              }else{
                  id = empid;
              }
              from = startDate.getText().toString();
              to = endDate.getText().toString();
              desc = leaveMessage.getText().toString();
              createdBy = empid;
            Map<String,String> imap = new HashMap<>();
              imap.put("empid",id);
              imap.put("leavetid", String.valueOf(tid));
              imap.put("leavefrom",from);
              imap.put("leaveto",to);
              imap.put("leavedescription",desc);
              imap.put("createby",createdBy);
            return imap;
          }
        };
        requestQueue.add(stringRequest);
    }


    private void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) Objects.requireNonNull(getActivity()).getSystemService(Activity.INPUT_METHOD_SERVICE);
        Objects.requireNonNull(inputMethodManager).hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private class GetDropDown extends AsyncTask<Void, Void, Void> {
        List<String> names;

        public GetDropDown(List<String> names) {
            this.names = names;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            sharedPreferences = getActivity().getSharedPreferences("Jankari", Context.MODE_PRIVATE);
            String empid = sharedPreferences.getString("userid", "");
            String url = "http://api.jankari.org.in/api/JankariPortal/FillDropDown?tablename=Employee&column1=distinct Employee.firstname%2B%27 %27%2BEmployee.lastname&column2=Employee.empid&whr=inner join EmployeeProgram on Employee.empid=EmployeeProgram.empid where EmployeeProgram.reportto='" + empid + "' and Employee.status=0";
            try {
                JSONHandler jsonHandler = new JSONHandler();
                String jsonStr = jsonHandler.makeJSONCall(url);
                if (jsonStr != null) {
                    JSONArray jsonArray = new JSONArray(jsonStr);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
//                        names.add(jsonObject.getString("column1"));
                        map.put(jsonObject.getString("column2"), jsonObject.getString("column1"));
                        names.add(map.get(jsonObject.getString("column2")));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, names);
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            empSpinner.setAdapter(arrayAdapter);
        }
    }
}
