package com.jankariportal.ui.profile;

public class Config {
    // File upload url (replace the ip with your server address)
    public static final String FILE_UPLOAD_URL = "http://192.168.0.199/AndroidImageUpload/upload.php";
//    public static final String FILE_UPLOAD_URL = "http://api.jankari.org.in/api/AddAttendanceinPhoto/emp154/2020-02-11";

    // Directory name to store captured images and videos
    public static final String IMAGE_DIRECTORY_NAME = "AndroidImageUpload";
}