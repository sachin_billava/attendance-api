package com.jankariportal.ui.profile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jankariportal.JSONHandler;
import com.jankariportal.MainActivity;
import com.jankariportal.R;
import com.jankariportal.custom.Utility;
import com.meg7.widget.CircleImageView;
import com.squareup.picasso.Picasso;

import org.jibble.simpleftp.SimpleFTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.jankariportal.custom.NetworkDetails.isNetworkAvailable;

public class ProfileFragment extends Fragment {

    TextView name, designation, salary, shift_timing, department, etmobile, email;
    CircleImageView profile_image;
    Map<String, String> data;
    View root;
    private int REQUEST_GALLERY_PHOTO = 1;
    private View dialogView;
    private AlertDialog.Builder builder;
    private AlertDialog dialog;
    ImageView edit;
    FloatingActionButton changeProIcon;
    String empId, pass, empDesignation, empDepartment, empShift, empEmail, empMobile, urlEmpDetails, urlPhotoUpdate, urlMobileUpdate;
    Button btnUpdate;
    Bitmap bitmap;
    View progressLayout;
    private String profilePic, empphoto;
    private static final int REQUEST_PERMISSIONS = 10;
    File destination;
    LottieAnimationView imageView;
    private Uri fileUri; // file url to store image/video
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private String upload_URL = "http://api.jankari.org.in/api/JankariPortal/UploadEMPPhoto/EMP154";
    private RequestQueue rQueue;
    private ArrayList<HashMap<String, String>> arraylist;
    private static final String TAG = MainActivity.class.getSimpleName();
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String userChoosenTask;
    //    Uploading Image via FTP
    public static final int REQUEST_EXTERNAL_PERMISSION_CODE = 666;
    Bitmap resizedImage;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public static final String[] PERMISSIONS_EXTERNAL_STORAGE = {READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE};

    @SuppressLint("RtlHardcoded")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences("Jankari", Context.MODE_PRIVATE);
        empId = sharedPreferences.getString("userid", "");
        pass = sharedPreferences.getString("pass", "");
        urlEmpDetails = "http://api.jankari.org.in/api/JankariPortal/GetAPPEmployeeDetails/" + empId; //+ empId;
        urlMobileUpdate = "http://api.jankari.org.in/api/JankariPortal/UpdateEmployee/" + empId;

        root = inflater.inflate(R.layout.fragment_profile, container, false);
        progressLayout = root.findViewById(R.id.progress_layout);
//        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
//        container.setScrollContainer(true);
        builder = new AlertDialog.Builder(getContext());
        dialogView = inflater.inflate(R.layout.update_mobile_alert, null);
        builder.setCancelable(true);
        builder.setView(dialogView);
        initialize();

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        return root;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.leave);
        if (item != null)
            item.setVisible(false);
    }

    private void initialize() {
        name = root.findViewById(R.id.name);
        designation = root.findViewById(R.id.designation);
//        salary = root.findViewById(R.id.salary);
//        shift_timing = root.findViewById(R.id.shift);
        department = root.findViewById(R.id.department);
        etmobile = root.findViewById(R.id.mobile_no);
        email = root.findViewById(R.id.email);
        profile_image = root.findViewById(R.id.profilePicture);
        dialog = builder.create();
        changeProIcon = root.findViewById(R.id.changeProIcon);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        changeProIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                    if ((ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) && (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.READ_EXTERNAL_STORAGE))) {

                    } else {
                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                                REQUEST_PERMISSIONS);
                    }
                } else {
                    Log.e("Else", "Else");
                    showPictureDialog();
//                    selectImage();
                }
//                showPictureDialog();

            }
        });

        // Checking camera availability
        if (!isDeviceSupportCamera()) {
            Toast.makeText(getActivity().getApplicationContext(),
                    "Sorry! Your device doesn't support camera",
                    Toast.LENGTH_LONG).show();
            // will close the app if the device does't have camera
            getActivity().finish();
        }

        edit = root.findViewById(R.id.edit);
        data = new HashMap<>();
        new GetValuesFromJSON().execute();

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMobileDialog(etmobile.getText().toString().trim());
//                updatePhoto();
            }
        });
    }

    /**
     * Checking device has camera hardware or not
     */
    private boolean isDeviceSupportCamera() {
        if (getActivity().getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    private void openMobileDialog(String number) {
        dialog.show();
        final EditText numberET = dialogView.findViewById(R.id.d_mobile);
        numberET.setText(number);
        Button cancel, update;
        cancel = dialogView.findViewById(R.id.d_cancel);
        update = dialogView.findViewById(R.id.d_update);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                etmobile.setText(numberET.getText().toString().trim());
                updateMobile();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getContext());
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        boolean result = Utility.checkPermission(getActivity().getApplicationContext());
                        switch (which) {
                            case 0:
//                                showFileChooser();
                                if (result)
                                    galleryIntent();
                                break;
                            case 1:
//                                captureImage();
                                if (result)
                                    cameraIntent();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream oGbytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, oGbytes);
        profilePic = empId + "_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime()) + ".jpg";
        destination = new File(Environment.getExternalStorageDirectory(), profilePic);
        ByteArrayOutputStream thumbbytes = new ByteArrayOutputStream();
        Bitmap thumb = Bitmap.createScaledBitmap(thumbnail,200,200,true);
        thumb.compress(Bitmap.CompressFormat.JPEG,90,thumbbytes);
//        Toast.makeText(ProfileActivity.this, destination.toString(), Toast.LENGTH_LONG).show();
        File thumbDest = new File(Environment.getExternalStorageDirectory(),profilePic);
        FileOutputStream thumbOut;
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(oGbytes.toByteArray());
            fo.close();
            thumbDest.createNewFile();
            thumbOut = new FileOutputStream(thumbDest);
            thumbOut.write(thumbbytes.toByteArray());
            thumbOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }



        profile_image.setImageBitmap(thumbnail);

        if (checkExternalStoragePermission(getActivity())) {
            // Continue with your action after permission request succeed
            UploadProfilePic async = new UploadProfilePic();

            if (isNetworkAvailable(getActivity())) {
                async.execute(destination.toString());
                new UploadThumb().execute(thumbDest.toString());
            } else {
                Toast.makeText(getActivity(), getString(R.string.no_working_internet_msg), Toast.LENGTH_LONG).show();
            }
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        profile_image.setImageBitmap(bm);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        ByteArrayOutputStream thumbBytes = new ByteArrayOutputStream();
        Bitmap thumb = Bitmap.createScaledBitmap(bm,200,200,true);
        thumb.compress(Bitmap.CompressFormat.JPEG,90,thumbBytes);
        profilePic = empId + "_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime()) + ".jpg";

        File destination = new File(Environment.getExternalStorageDirectory(), profilePic);
        File thumbDest = new File(Environment.getExternalStorageDirectory(), profilePic);
        FileOutputStream fo;
        FileOutputStream thumbOut;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            thumbOut = new FileOutputStream(thumbDest);

            fo.write(bytes.toByteArray());
            fo.close();
            thumbOut.write(thumbBytes.toByteArray());
            thumbOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (checkExternalStoragePermission(getActivity())) {
            // Continue with your action after permission request succeed
            UploadProfilePic async = new UploadProfilePic();

            if (isNetworkAvailable(getActivity())) {
                async.execute(destination.toString());
                new UploadThumb().execute(thumbDest.toString());
            } else {
                Toast.makeText(getActivity(), getString(R.string.no_working_internet_msg), Toast.LENGTH_LONG).show();
            }
        }

    }

    public boolean checkExternalStoragePermission(Activity activity) {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            return true;
        }

        int readStoragePermissionState = ContextCompat.checkSelfPermission(activity, READ_EXTERNAL_STORAGE);
        int writeStoragePermissionState = ContextCompat.checkSelfPermission(activity, WRITE_EXTERNAL_STORAGE);
        boolean externalStoragePermissionGranted = readStoragePermissionState == PackageManager.PERMISSION_GRANTED &&
                writeStoragePermissionState == PackageManager.PERMISSION_GRANTED;
        if (!externalStoragePermissionGranted) {
            requestPermissions(PERMISSIONS_EXTERNAL_STORAGE, REQUEST_EXTERNAL_PERMISSION_CODE);
        }

        return externalStoragePermissionGranted;
    }
    class UploadThumb extends AsyncTask<String, Integer, String>{


        @Override
        protected String doInBackground(String... params) {
            // ftpClient=uploadingFilestoFtp();
            try {
                SimpleFTP ftp = new SimpleFTP();

                ftp.connect(getResources().getString(R.string.ftp_host_name), Integer.parseInt(getResources().getString(R.string.ftp_port)), getResources().getString(R.string.ftp_username), getResources().getString(R.string.ftp_password));

                ftp.bin();

                // Change to a new working directory on the FTP server.
                ftp.cwd("api.jankari.org.in/JankariContent/ThumbImage");

                File extStore = Environment.getExternalStorageDirectory();
                String mPath = extStore.getAbsolutePath();
                // Upload some files.
                ftp.stor(new File(params[0]));
//                ftp.stor(new File(mPath + "/1514602502208.jpg"));

                ftp.disconnect();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
    class UploadProfilePic extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params) {
            // ftpClient=uploadingFilestoFtp();
            try {
                SimpleFTP ftp = new SimpleFTP();

                ftp.connect(getResources().getString(R.string.ftp_host_name), Integer.parseInt(getResources().getString(R.string.ftp_port)), getResources().getString(R.string.ftp_username), getResources().getString(R.string.ftp_password));

                ftp.bin();

                // Change to a new working directory on the FTP server.
                ftp.cwd(getResources().getString(R.string.post_profilepic_path));

                File extStore = Environment.getExternalStorageDirectory();
                String mPath = extStore.getAbsolutePath();
                // Upload some files.
                ftp.stor(new File(params[0]));
//                ftp.stor(new File(mPath + "/1514602502208.jpg"));

                ftp.disconnect();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //TODO show dailog
            progressLayout.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String result) {
//            Toast.makeText(ProfileActivity.this, "Success", Toast.LENGTH_LONG).show();
//            String url = getResources().getString(R.string.api_main_url) + getResources().getString(R.string.api_value_url) + "/UpdateuserDetails";
//
            if (isNetworkAvailable(getActivity())) {
//                new PostMyData().execute(url);
                updateMobile();
            } else {
                Toast.makeText(getActivity(), getString(R.string.no_working_internet_msg), Toast.LENGTH_LONG).show();
            }
        }
    }

    //Mobile number update
    private void updateMobile() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String date = sdf.format(now);
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("mobile", etmobile.getText().toString().trim());
            jsonObject.put("empphoto", profilePic);
            jsonObject.put("updatedby", empId);
            jsonObject.put("updatedon", date);

            final String requestBody = jsonObject.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, urlMobileUpdate, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("VOLLEY", response);
                    progressLayout.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "Updated Successfully", Toast.LENGTH_SHORT).show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                    dialog.dismiss();
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setData() {
        name.setText(data.get("name"));
        designation.setText(data.get("designation"));
//        shift_timing.setText(data.get("shift_and_timing"));
        department.setText(data.get("department"));
        etmobile.setText(data.get("mobile"));
        email.setText(data.get("email"));
        profilePic = data.get("empphoto");

        if (profilePic.equals("null") || profilePic.equals(null) || profilePic.equals("")) {

        } else {
            Picasso.get().load(getResources().getString(R.string.domain_url) + getResources().getString(R.string.get_profilepic_path) + profilePic)
                    .fit()
                    .centerCrop(Gravity.TOP)
                    .noFade()
                    .into(profile_image);
        }

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("Jankari", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("emp", String.valueOf(name));
        editor.putString("email", String.valueOf(email));
        editor.putString("empphoto", profilePic);
        editor.commit();
    }


    private class GetValuesFromJSON extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            JSONHandler jsonHandler = new JSONHandler();
            String jsonStr = jsonHandler.makeJSONCall(urlEmpDetails);
            if (jsonStr != null) {
                try {
                    JSONArray jsonArray = new JSONArray(jsonStr);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        data.put("name", jsonObject.getString("empname"));
                        data.put("designation", jsonObject.getString("desname"));
                        data.put("empphoto", jsonObject.getString("empphoto"));
//                        data.put("shift_and_timing", jsonObject.getString("shift_and_timing"));
                        data.put("department", jsonObject.getString("depname"));
                        data.put("email", jsonObject.getString("offemail"));
                        data.put("mobile", jsonObject.getString("mobile"));
                    }
//                    Log.e("object", String.valueOf(data));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getContext(), "PROBLEM!!!", Toast.LENGTH_SHORT).show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            setData();
        }
    }
}
