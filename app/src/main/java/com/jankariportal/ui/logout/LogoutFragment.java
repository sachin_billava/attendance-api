package com.jankariportal.ui.logout;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.jankariportal.LoginActivity;
import com.jankariportal.MainActivity;
import com.jankariportal.R;

import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

public class LogoutFragment extends DialogFragment {
    private AlertDialog.Builder builder;
    private View dialogView;
    static final String PREFS_NAME = "Jankari";
    private SharedPreferences mPrefs;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
setHasOptionsMenu(true);
        View root = inflater.inflate(R.layout.fragment_logout, container, false);
        builder = new AlertDialog.Builder(getContext());
        dialogView = inflater.inflate(R.layout.logout_dialog,null);
        builder.setCancelable(false);
        builder.setView(dialogView);
        final AlertDialog dialog = builder.create();
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        mPrefs = getActivity().getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        Button cancel,logout;
        cancel = dialogView.findViewById(R.id.lg_cancel);
        logout = dialogView.findViewById(R.id.lg_logout);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                dialog.dismiss();
                getActivity().finish();
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = mPrefs.edit();
                editor.clear();
                editor.commit();
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                dialog.dismiss();
                getActivity().finish();
            }
        });
        return root;
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item=menu.findItem(R.id.leave);
        if(item!=null)
            item.setVisible(false);
    }
}