package com.jankariportal.ui.dashboard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.jankariportal.R;
import com.jankariportal.ui.empattendance.EmployeeAttendanceLog;
import com.jankariportal.ui.attendance.AttendanceFragment;
import com.jankariportal.ui.leaves.LeavesFragment;
import com.jankariportal.ui.visit.VisitFragment;

public class DashboardFragment extends Fragment {

    ActionBar actionBar;
    private View root;
    private BottomNavigationView bottomNavigationView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_dashboard, container, false);
        container.setScrollContainer(true);
        bottomNavigationView = root.findViewById(R.id.bottom_nav);
//        bottomNavigationView.getMenu().getItem(1).setChecked(true);
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        openFragment(new AttendanceFragment());
        bottomNavigationView.setSelectedItemId(R.id.navigation_attendance);
        actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        return root;
    }

    private void openFragment(Fragment fragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
//        transaction.setCustomAnimations(R.anim.fromtop,R.anim.fadeout);
//        fragment.setEnterTransition(new Slide(Gravity.TOP));
//        fragment.setExitTransition(new Fade(Fade.MODE_OUT));
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.navigation_leave:
                            openFragment(new LeavesFragment());
                            actionBar.setTitle(R.string.title_leave);
                            return true;
                        case R.id.navigation_attendance:
                            openFragment(new AttendanceFragment());
//                            actionBar.setTitle(R.string.title_attendance);
                            return true;
                        case R.id.navigation_visit:
                            openFragment(new VisitFragment());
                            actionBar.setTitle(R.string.title_visit);
                            return true;
                        case R.id.nav_emplog:
                            openFragment(new EmployeeAttendanceLog());
                            actionBar.setTitle(R.string.empattlog);
                            return true;
                    }
                    return false;
                }
            };

    @Override
    public void onResume() {
        super.onResume();
        openFragment(new AttendanceFragment());
        bottomNavigationView.setSelectedItemId(R.id.navigation_attendance);
    }
}