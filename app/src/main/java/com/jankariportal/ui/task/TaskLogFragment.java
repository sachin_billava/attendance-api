package com.jankariportal.ui.task;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jankariportal.AddTaskActivity;
import com.jankariportal.JSONHandler;
import com.jankariportal.R;
import com.jankariportal.Task;
import com.jankariportal.TaskAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class TaskLogFragment extends Fragment {
    RecyclerView recyclerView;
    FloatingActionButton addBtn,searchBtn;
    SharedPreferences sharedPreferences;
    String empid;
    private View dialogView;
    TaskAdapter adapter ;
    Context context;
    List<Task> taskList;
    SwipeRefreshLayout swipeRefreshLayout;
    private AlertDialog.Builder builder;
    private AlertDialog dialog;
    Calendar calendar;
    View progressLayout;
    public TaskLogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        View view=inflater.inflate(R.layout.fragment_task_log, container, false);
        taskList = new ArrayList<>();
        context = getContext();
        adapter = new TaskAdapter(taskList,context);
        sharedPreferences = getActivity().getSharedPreferences("Jankari",MODE_PRIVATE);
        empid = sharedPreferences.getString("userid","");
        swipeRefreshLayout = view.findViewById(R.id.swipe_continer);
        recyclerView = view.findViewById(R.id.taskRecycler);
        addBtn = view.findViewById(R.id.floatingActionButton2);
        searchBtn = view.findViewById(R.id.search);
        progressLayout = view.findViewById(R.id.progress_layout);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        calendar = Calendar.getInstance();
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity().getApplicationContext(),DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getActivity().getApplicationContext(),R.drawable.divider));
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(adapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                        // Stop animation (This will be after 3 seconds)
                        swipeRefreshLayout.setRefreshing(false);
                        progressLayout.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                taskList.clear();
                                new GetValuesFromJson().execute();
                                Toast.makeText(getActivity(), "Tasks Refreshed!", Toast.LENGTH_LONG).show();
                            }
                        },500);
            }
        });
        builder = new AlertDialog.Builder(getContext());
        dialogView = inflater.inflate(R.layout.search_dialog, null);
        builder.setCancelable(true);
        builder.setView(dialogView);
        dialog = builder.create();
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buildDailoague();
            }
        });
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), AddTaskActivity.class);
                startActivity(i);
            }
        });
        new GetValuesFromJson().execute();
        return view;
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item=menu.findItem(R.id.leave);
        if(item!=null)
            item.setVisible(false);
    }
    private void buildDailoague() {
        dialog.show();
        final int[] posi = new int[1];
        final EditText date = dialogView.findViewById(R.id.search_date);
        final Spinner spinner = dialogView.findViewById(R.id.search_spinner);
        final Button search = dialogView.findViewById(R.id.d_search);
        Button cancel = dialogView.findViewById(R.id.d_cancel);
        final DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR,year);
                calendar.set(Calendar.MONTH,month);
                calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                updateEt(date);
            }

        };
        List<String> list = new ArrayList<>();
        list.add("Select Month");
        list.add("January");
        list.add("February");
        list.add("March");
        list.add("April");
        list.add("May");
        list.add("June");
        list.add("July");
        list.add("August");
        list.add("September");
        list.add("October");
        list.add("November");
        list.add("December");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, list);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getContext(),dateSetListener,calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                posi[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                taskList.clear();
                if(date.getText().toString().length() > 0){

                    dialog.dismiss();
                    progressLayout.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            new GetValues(date.getText().toString()).execute();
                            date.setText("");
                        }
                    },500);
                }
                if(spinner.getSelectedItemPosition() != 0){

                    dialog.dismiss();
                    progressLayout.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            new GetValues(posi[0]).execute();
                            date.setText("");
                        }
                    },500);
                }
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        progressLayout.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                taskList.clear();
                new GetValuesFromJson().execute();
//                Toast.makeText(getActivity(), "Tasks Refreshed!", Toast.LENGTH_LONG).show();
            }
        },500);
    }

    private class GetValues extends AsyncTask<Void,Void,Void> {
        String date;
        int pos;
        boolean norecord = false;

        public GetValues(String date) {
            this.date = date;
        }

        public GetValues(int pos) {
            this.pos = pos;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            JSONHandler jsonHandler = new JSONHandler();

            String jsonStr = jsonHandler.makeJSONCall("http://api.jankari.org.in/api/JankariPortal/GetUserDailyTaskList?whr=And umain.userid = '"+empid+"'");
            if (jsonStr != null){
                try {
                    JSONArray jsonArray = new JSONArray(jsonStr);
                    for (int i=0;i<jsonArray.length();i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String date, summary;
                        int count;
                        String posMonth;
                        date = jsonObject.getString("reportdate").substring(0, 10);
                        posMonth = jsonObject.getString("reportdate").substring(5, 7);
                        Log.e("pos",posMonth);
                        Date newD = new SimpleDateFormat("yyyy-MM-dd").parse(date);
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MMM-dd");
                        String newDate = simpleDateFormat.format(newD);
                        count = jsonObject.getInt("tottasks");
                        summary = "Summary";

                        if (this.date != null && date.equals(this.date)){
                            Task task = new Task(newDate, summary, count);
                            taskList.add(task);
                        }
                        if (this.pos > 0 && this.pos == Integer.parseInt(posMonth)) {
                            Task task = new Task(newDate, summary, count);
                            taskList.add(task);
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressLayout.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            adapter.notifyDataSetChanged();
            progressLayout.setVisibility(View.GONE);
            if (taskList.size() == 0){
                norecord = true;
            }
            if (norecord){
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("No record found...");
                builder.setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                new GetValuesFromJson().execute();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.setTitle("Sorry!!!");
                alert.show();
            }
        }
    }


    public void updateEt(EditText editText){
        String format = "yyyy-MM-dd";
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        String sdate = simpleDateFormat.format(calendar.getTime());
        editText.setText(sdate);
    }

    private class GetValuesFromJson extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            JSONHandler jsonHandler = new JSONHandler();
            String jsonStr = jsonHandler.makeJSONCall("http://api.jankari.org.in/api/JankariPortal/GetUserDailyTaskList?whr=And umain.userid = '"+empid+"'");
            if (jsonStr != null){
                try {
                    JSONArray jsonArray = new JSONArray(jsonStr);
                    for (int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String date,summary,name;
                        int count;
                        date = jsonObject.getString("reportdate").substring(0,10);

                        Date newD = new SimpleDateFormat("yyyy-MM-dd").parse(date);
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MMM-dd");
                        String newDate = simpleDateFormat.format(newD);

                        count = jsonObject.getInt("tottasks");
                        summary = "Summary";
                        Task task = new Task(newDate,summary,count);
                        taskList.add(task);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            adapter.notifyDataSetChanged();
            progressLayout.setVisibility(View.GONE);
        }
    }
}
