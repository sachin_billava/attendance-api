package com.jankariportal.ui.attendance;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jankariportal.AlarmNotificationReceiver;
import com.jankariportal.AppLocationService;
import com.jankariportal.AttendanceLogActivity;
import com.jankariportal.JSONHandler;
import com.jankariportal.LocationAddress;
import com.jankariportal.MainActivity;
import com.jankariportal.R;
import com.jankariportal.ShowSettingsAlert;
import com.jankariportal.ui.visit.VisitFragment;
import com.meg7.widget.CircleImageView;
import com.squareup.picasso.Picasso;

import org.jibble.simpleftp.SimpleFTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_CANCELED;
import static android.content.Context.ALARM_SERVICE;
import static android.content.Context.LOCATION_SERVICE;
import static android.content.Context.MODE_PRIVATE;
import static com.jankariportal.custom.NetworkDetails.isNetworkAvailable;
import static com.jankariportal.ui.profile.ProfileFragment.PERMISSIONS_EXTERNAL_STORAGE;
import static com.jankariportal.ui.profile.ProfileFragment.REQUEST_EXTERNAL_PERMISSION_CODE;


/**
 * A simple {@link Fragment} subclass.
 */
public class AttendanceFragment extends Fragment implements LocationListener {

    private int REQUEST_CAMERA = 0;
    CircleImageView takeImage;
    Button markIn, markOut;
    LocationManager locationManager;
    LocationListener locationListener;
    boolean flag;
    View root;
    int requestCode = 101;
    boolean lessThanNoug = false;
    ImageView refreshIcon;
    TextView addressTextView, clickToRefresh, userName;
    AppLocationService appLocationService;
    int mintime = 10000;
    int minDist = 100;
    private String empId = null, date = null, starttime = null, endtime = null, attendance_in_pic, attendance_out_pic, profilePic, empname,
            checkdate, checkintime, checkouttime;
    double latitude = 0;
    double longitude = 0;
    String urlAttendance = null, urlOutAttendance = null, pic, strlocationAddress, checklocation = "", iF = null, checkAttendanceStatus = "";
    File destination;
    LottieAnimationView lottieAnimationView;
    PendingIntent pendingIntent;

    public AttendanceFragment() {
        // Required empty public constructor
    }


    @SuppressLint("ResourceAsColor")
    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_attendance, container, false);
        takeImage = root.findViewById(R.id.profilePicture);
        clickToRefresh = root.findViewById(R.id.clickToRefresh);
        userName = root.findViewById(R.id.tvAttendanceWelcome);
        markIn = root.findViewById(R.id.markin);
        markOut = root.findViewById(R.id.markout);
        markOut.setVisibility(View.GONE);
        refreshIcon = root.findViewById(R.id.refresh);
        addressTextView = root.findViewById(R.id.address);
        lottieAnimationView = root.findViewById(R.id.lottieloading);
        locationListener = this;
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            lessThanNoug = true;
        } else {
            lessThanNoug = false;
        }
        appLocationService = new AppLocationService(getActivity());
        locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, requestCode);
        }

        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences("Jankari", MODE_PRIVATE);
        empname = sharedPreferences.getString("fullname", "");
        empId = sharedPreferences.getString("userid", "");
        new userDetails().execute();
        profilePic = sharedPreferences.getString("empphoto", "");

        checkAttendance();
        takeImage.setVisibility(View.INVISIBLE);
        addressTextView.setVisibility(View.INVISIBLE);

        //if attendeance marked then dont do
        if (lessThanNoug) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, mintime, minDist, this);
        } else {
            fetchAddress();
        }
//        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

        flag = true;
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle(R.string.title_attendance);

        if (profilePic.equals(null) || profilePic.equals("null") || profilePic.equals("")) {
//            takeImage.setImageResource(R.mipmap.ic_launcher);
        } else {
            Picasso.get().load(getResources().getString(R.string.domain_url) + getResources().getString(R.string.get_profilepic_path) + profilePic)
                    .fit()
                    .centerCrop(Gravity.TOP)
                    .noFade()
                    .into(takeImage);
        }

        //get current time
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        date = sdf.format(now);
        markIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check current day's attendance
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
                }
                captureImageFromCamera(v);
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            }
        });
        markOut.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View v) {
                captureImageFromCamera(v);
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            }
        });

        urlAttendance = "http://api.jankari.org.in/api/Employee/AddEMPAttendanceLog/" + empId;
        urlOutAttendance = "http://api.jankari.org.in/api/Employee/UpdateEMPAttendanceLog/" + empId;

        clickToRefresh.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View v) {
                Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate);
                refreshIcon.startAnimation(animation);
                if (lessThanNoug) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
                } else {
                    fetchAddress();
                }
            }
        });
        refreshIcon.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View v) {
                Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate);
                refreshIcon.startAnimation(animation);
                if (lessThanNoug) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
                } else {
                    fetchAddress();
                }
            }
        });
        FloatingActionButton fab = root.findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AttendanceLogActivity.class);
                startActivity(intent);
            }
        });

        return root;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onResume() {
        super.onResume();
        if (addressTextView.getText() == getString(R.string.attendance_success)) {
            addressTextView.setTextColor(R.color.colorPrimaryDark);
        }
    }

    private void fetchAddress() {
        Location location = appLocationService.getLocation(LocationManager.NETWORK_PROVIDER);
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            LocationAddress locationAddress = new LocationAddress();
            locationAddress.getAddressFromLocation(latitude, longitude, getActivity(), new GeocoderHandler());
        } else {
            new ShowSettingsAlert(getContext());

        }
    }


    private void captureImageFromCamera(View v) {
        if (v == markIn) {
            flag = true;
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, REQUEST_CAMERA);
        }
        if (v == markOut) {
            flag = false;
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, REQUEST_CAMERA);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        longitude = location.getLongitude();
        latitude = location.getLatitude();
        LocationAddress locationAddress = new LocationAddress();
        locationAddress.getAddressFromLocation(latitude, longitude, getActivity(), new GeocoderHandler());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        new ShowSettingsAlert(getContext());
    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {

            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    strlocationAddress = bundle.getString("address");
                    break;
                default:
                    strlocationAddress = null;
            }
            checkAttendance();
            if (checklocation.equals("Marked")) {

            } else {
                addressTextView.setText(strlocationAddress);
            }
        }
    }

    private void checkAttendance() {
        checkdate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        final String isFinal = getActivity().getIntent().getStringExtra("isFinal");
        if (isFinal == null) {

        } else {
            iF = isFinal;
        }
        RequestQueue queue = Volley.newRequestQueue(getContext());
        String url = "http://api.jankari.org.in/api/Employee/CheckAttlog/" + checkdate + "/" + empId;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    if (response.equalsIgnoreCase("\"Failure\"")) {
                        markIn.setVisibility(View.VISIBLE);
                        markOut.setVisibility(View.GONE);
                        refreshIcon.setVisibility(View.VISIBLE);
                        clickToRefresh.setVisibility(View.VISIBLE);
                        addressTextView.setVisibility(View.VISIBLE);
                        checkAttendanceStatus = "Failure";
                        checklocation = "Done";
                        startAlarm(true, true);
                    } else if (response.equalsIgnoreCase("\"Markedin\"")) {
                        markIn.setVisibility(View.GONE);
                        markOut.setVisibility(View.VISIBLE);
                        refreshIcon.setVisibility(View.VISIBLE);
                        addressTextView.setVisibility(View.VISIBLE);
                        clickToRefresh.setVisibility(View.VISIBLE);
                        checkAttendanceStatus = "Marked";
                        checklocation = "Done";
                    } else if (response.equalsIgnoreCase("\"Markedout\"") || iF.equalsIgnoreCase("\"attMarkedout\"")) {
                        markIn.setVisibility(View.GONE);
                        markOut.setVisibility(View.GONE);
                        addressTextView.setVisibility(View.VISIBLE);
                        addressTextView.setText(getString(R.string.attendance_success));
                        checklocation = "Marked";
                        checkAttendanceStatus = "Marked";
                        refreshIcon.setVisibility(View.GONE);
                        clickToRefresh.setVisibility(View.GONE);
                    }
                    takeImage.setVisibility(View.VISIBLE);
                    userName.setText(empname);
                } catch (
                        Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(stringRequest);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("Code", String.valueOf(requestCode));
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (requestCode == REQUEST_CAMERA) {
            if (flag) {
                onCaptureImageResult(data);
                markIn.setVisibility(View.GONE);
                markOut.setVisibility(View.VISIBLE);
//                addInAttendance();

                if (checkExternalStoragePermission(getActivity())) {
                    // Continue with your action after permission request succeed
                    AttendanceFragment.UploadAttendancePic async = new AttendanceFragment.UploadAttendancePic();

                    if (isNetworkAvailable(getActivity())) {
                        async.execute(destination.toString());
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.no_working_internet_msg), Toast.LENGTH_LONG).show();
                    }
                }
            } else {
                try {
                    onCaptureImageResult1(data);
                    markIn.setVisibility(View.GONE);
                    markOut.setVisibility(View.GONE);
//                    addOutAttendance();
                    if (checkExternalStoragePermission(getActivity())) {
                        // Continue with your action after permission request succeed
                        AttendanceFragment.UploadAttendanceOutPic async = new AttendanceFragment.UploadAttendanceOutPic();

                        if (isNetworkAvailable(getActivity())) {
                            async.execute(destination.toString());
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.no_working_internet_msg), Toast.LENGTH_LONG).show();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
//            Toast.makeText(getContext(), "Image Saved!", Toast.LENGTH_SHORT).show();
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        attendance_in_pic = empId + "_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime()) + ".jpg";

        destination = new File(Environment.getExternalStorageDirectory(), attendance_in_pic);

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
//            destination.delete();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void onCaptureImageResult1(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        attendance_out_pic = empId + "_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime()) + ".jpg";

        destination = new File(Environment.getExternalStorageDirectory(), attendance_out_pic);

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
//            destination.delete();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean checkExternalStoragePermission(Activity activity) {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            return true;
        }

        int readStoragePermissionState = ContextCompat.checkSelfPermission(activity, READ_EXTERNAL_STORAGE);
        int writeStoragePermissionState = ContextCompat.checkSelfPermission(activity, WRITE_EXTERNAL_STORAGE);
        boolean externalStoragePermissionGranted = readStoragePermissionState == PackageManager.PERMISSION_GRANTED &&
                writeStoragePermissionState == PackageManager.PERMISSION_GRANTED;
        if (!externalStoragePermissionGranted) {
            requestPermissions(PERMISSIONS_EXTERNAL_STORAGE, REQUEST_EXTERNAL_PERMISSION_CODE);
        }

        return externalStoragePermissionGranted;
    }

    class UploadAttendancePic extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            // ftpClient=uploadingFilestoFtp();
            try {
                SimpleFTP ftp = new SimpleFTP();

                ftp.connect(getResources().getString(R.string.ftp_host_name), Integer.parseInt(getResources().getString(R.string.ftp_port)), getResources().getString(R.string.ftp_username), getResources().getString(R.string.ftp_password));

                ftp.bin();

                // Change to a new working directory on the FTP server.
                ftp.cwd(getResources().getString(R.string.post_attendanceinpic_path));

                File extStore = Environment.getExternalStorageDirectory();
                String mPath = extStore.getAbsolutePath();
                // Upload some files.
                ftp.stor(new File(params[0]));

                ftp.disconnect();

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            if (isNetworkAvailable(getActivity())) {
                addInAttendance();
            } else {
                Toast.makeText(getActivity(), getString(R.string.no_working_internet_msg), Toast.LENGTH_LONG).show();
            }
        }
    }

    class UploadAttendanceOutPic extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            // ftpClient=uploadingFilestoFtp();
            try {
                SimpleFTP ftp = new SimpleFTP();

                ftp.connect(getResources().getString(R.string.ftp_host_name), Integer.parseInt(getResources().getString(R.string.ftp_port)), getResources().getString(R.string.ftp_username), getResources().getString(R.string.ftp_password));

                ftp.bin();

                // Change to a new working directory on the FTP server.
                ftp.cwd(getResources().getString(R.string.post_attendanceoutpic_path));

                File extStore = Environment.getExternalStorageDirectory();
                String mPath = extStore.getAbsolutePath();
                // Upload some files.
                ftp.stor(new File(params[0]));

                ftp.disconnect();

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {

            if (isNetworkAvailable(getActivity())) {
                addOutAttendance();
            } else {
                Toast.makeText(getActivity(), getString(R.string.no_working_internet_msg), Toast.LENGTH_LONG).show();
            }
        }

    }

    private void addInAttendance() {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("empid", empId);
            jsonObject.put("date", date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date()));
            jsonObject.put("starttime", starttime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date()));
            jsonObject.put("startloclongitude", longitude);
            jsonObject.put("startloclatitude", latitude);
            jsonObject.put("photoin", attendance_in_pic);
            jsonObject.put("createby", empId);
            jsonObject.put("createon", date);

            final String requestBody = jsonObject.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, urlAttendance, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("VOLLEY", response);
                    Toast.makeText(getActivity(), "Attendance Marked In Successfully", Toast.LENGTH_SHORT).show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
//                    dialog.dismiss();
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addOutAttendance() {
        try {
            endtime = null;
//            date = null;
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("empid", empId);
            jsonObject.put("date", date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date()));
            jsonObject.put("endtime", endtime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date()));
            jsonObject.put("endloclongitude", longitude);
            jsonObject.put("endloclatitude", latitude);
            jsonObject.put("photoout", attendance_out_pic);
            jsonObject.put("updateby", empId);
            jsonObject.put("updateon", date);

            final String requestBody = jsonObject.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, urlOutAttendance, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("VOLLEY", response);
                    Toast.makeText(getActivity(), "Attendance Marked Out Successfully", Toast.LENGTH_SHORT).show();
                    addressTextView.setText(getString(R.string.attendance_success));
//                    addressTextView.setTextColor(R.color.colorPrimaryDark);
                    refreshIcon.setVisibility(View.GONE);
                    clickToRefresh.setVisibility(View.GONE);
                    destination.delete();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
//                    dialog.dismiss();
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class userDetails extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            String urlEmpDetails = "http://api.jankari.org.in/api/JankariPortal/GetAPPEmployeeDetails/" + empId;
            JSONHandler jsonHandler = new JSONHandler();
            String jsonStr = jsonHandler.makeJSONCall(urlEmpDetails);
            if (jsonStr != null) {
                try {
                    JSONArray jsonArray = new JSONArray(jsonStr);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        pic = jsonObject.getString("empphoto");

                        SharedPreferences sharedPreferences = getActivity().getApplicationContext().getSharedPreferences("Jankari", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("empphoto", pic);
                        editor.apply();
                    }
//                    Log.e("object", String.valueOf(data));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getContext(), "PROBLEM!!!", Toast.LENGTH_SHORT).show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    private void startAlarm(boolean isNotification, boolean isRepeat) {
        AlarmManager manager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
        Intent myIntent;

        // SET TIME HERE
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 10);
        calendar.set(Calendar.MINUTE, 20);

        checkAttendance();
        if (checkAttendanceStatus.equalsIgnoreCase("failure")||checkAttendanceStatus.equals("")||checkAttendanceStatus.equals(null)) {
            myIntent = new Intent(getActivity().getApplicationContext(), AlarmNotificationReceiver.class);
            pendingIntent = PendingIntent.getBroadcast(getActivity().getApplicationContext(), 0, myIntent, 0);
        }

        if (calendar.getTime().compareTo(new Date()) < 0) calendar.add(Calendar.DAY_OF_MONTH, 1);

        if (!isRepeat)
            manager.set(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime() + 3000, pendingIntent);
        else
            manager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
    }
}
