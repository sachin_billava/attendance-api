package com.jankariportal.ui.changepassword;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.jankariportal.LoginActivity;
import com.jankariportal.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Objects;

public class ChangePasswordFragment extends Fragment {
    public ChangePasswordFragment() {
    }

    private EditText oldPass,newPass,confirmNewPass;
    private SharedPreferences sharedPreferences;
    private View progressLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View root = inflater.inflate(R.layout.fragment_change_password, container, false);
        oldPass = root.findViewById(R.id.old_pass);
        newPass  = root.findViewById(R.id.new_pass);
        confirmNewPass = root.findViewById(R.id.confirm_new_pass);
        progressLayout = root.findViewById(R.id.progress_layout);
        Button changePasswordBtn = root.findViewById(R.id.change_password);
        sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences("Jankari", Context.MODE_PRIVATE);
        changePasswordBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                changePassword();
            }
        });
        oldPass.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
        newPass.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
        confirmNewPass.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
        return root;
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item=menu.findItem(R.id.leave);
        if(item!=null)
            item.setVisible(false);
    }
    private void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager) Objects.requireNonNull(getActivity()).getSystemService(Activity.INPUT_METHOD_SERVICE);
        Objects.requireNonNull(inputMethodManager).hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void changePassword() {
        Animation shake = AnimationUtils.loadAnimation(getContext(), R.anim.shake);
        String soldPass,snewPass,sconfirmNewPass;
        soldPass = oldPass.getText().toString().trim();
        snewPass = newPass.getText().toString().trim();
        sconfirmNewPass = confirmNewPass.getText().toString().trim();
        boolean flag;
        if (soldPass.length() != 0){
            if (snewPass.length() != 0){
                if (sconfirmNewPass.length() != 0){
                    flag = true;
                }else {
                    flag = false;
                    confirmNewPass.setError("Should Not be Empty");
                    confirmNewPass.startAnimation(shake);
                }
            }else{
                flag = false;
                newPass.setError("Should Not be Empty");
                newPass.startAnimation(shake);
            }
        }else{
            flag = false;
            oldPass.setError("Should Not be Empty");
            oldPass.startAnimation(shake);
        }

        if (flag){
            if (soldPass.equals(sharedPreferences.getString("pass", ""))){
                if (snewPass.equals(sconfirmNewPass)){
                    if (snewPass.length() >= 3){
                        String userId = sharedPreferences.getString("userid","");
                        String url = "http://api.jankari.org.in/api/JankariPortal/UpdatePassword/"+userId;
                        Log.e("url",url);
                        RequestQueue requestQueue = Volley.newRequestQueue(Objects.requireNonNull(getContext()));
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("password",snewPass);
                            final String requestBody = jsonObject.toString();
                            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.i("VOLLEY", response);
                                    Toast.makeText(getActivity(), "Updated Successfully", Toast.LENGTH_SHORT).show();
                                    @SuppressLint("CommitPrefEdits")
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.clear();
                                    editor.apply();
                                    Intent intent = new Intent(getContext(), LoginActivity.class);
                                    startActivity(intent);
                                    progressLayout.setVisibility(View.GONE);
                                    Objects.requireNonNull(getActivity()).finish();
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.e("VOLLEY", error.toString());
                                    progressLayout.setVisibility(View.GONE);
//                                    dialog.dismiss();
                                }
                            }){
                                @Override
                                public String getBodyContentType() {
                                    return "application/json; charset=utf-8";
                                }

                                @Override
                                public byte[] getBody() throws AuthFailureError {
                                    try {
                                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                                    } catch (UnsupportedEncodingException uee) {
                                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                        return null;
                                    }
                                }

                                @Override
                                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                                    String responseString = "";
                                    if (response != null) {
                                        responseString = String.valueOf(response.statusCode);
                                        // can get more details such as response.headers
                                    }
                                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                                }
                            };
                            progressLayout.setVisibility(View.VISIBLE);
                            requestQueue.add(stringRequest);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }else{
                        newPass.setError("Password At Least Have 8 Alphabets");
                        newPass.startAnimation(shake);
                    }
                }else{
                    newPass.setError("Password Didn't Matched");
                    newPass.startAnimation(shake);
                    confirmNewPass.setError("Password Didn't Matched");
                    confirmNewPass.startAnimation(shake);
                }
            }else{
                oldPass.setError("Old Password Is Wrong");
                oldPass.startAnimation(shake);
            }
        }

    }
}
