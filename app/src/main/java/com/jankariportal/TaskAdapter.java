package com.jankariportal;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.Volley;
import com.jankariportal.ui.task.TaskLogFragment;

import java.util.ArrayList;
import java.util.List;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.ViewHolder> {
    private List<Task> list  = new ArrayList<>();
    private Context context;

    public TaskAdapter(List<Task> list,Context context) {
        this.list = list;
        this.context = context;
    }
    class ViewHolder extends RecyclerView.ViewHolder {

        TextView date,tCount,summary;

        ViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.task_date);
            tCount = itemView.findViewById(R.id.task_count);
            summary = itemView.findViewById(R.id.summary);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_task, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Task task = list.get(position);
        holder.date.setText(task.getDate());
        holder.tCount.setText(String.valueOf(task.getTaskCount()));
        holder.summary.setText(task.getSummary());
        holder.summary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,TaskSummaryActivity.class);
                intent.putExtra("date",task.getDate());
                intent.putExtra("count",task.getTaskCount());
                context.startActivity(intent);
            }
        });
        holder.date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,AddTaskActivity.class);
                intent.putExtra("reportdate",task.getDate());
                context.startActivity(intent);
            }
        });
    }

}
