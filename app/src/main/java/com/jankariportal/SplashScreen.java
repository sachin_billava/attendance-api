package com.jankariportal;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.util.Timer;
import java.util.TimerTask;

public class SplashScreen extends AppCompatActivity {

    Context context;
    static final String PREFS_NAME = "Jankari";
    ImageView ivSplash;
    private int i = 0;
    AlertDialog.Builder builder;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        ivSplash = findViewById(R.id.ivSplash);
//        progressBar = findViewById(R.id.progressBar);
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.splash);
        ivSplash.startAnimation(anim);
//        progressBar.setProgress(0);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        final long period = 10;
        final Timer timer = new Timer();

        if (!isOnline(SplashScreen.this)) buildDialog(SplashScreen.this).show();

        else {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    SharedPreferences sp = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
                    if (sp.contains("pref_name")) {
                        Intent mIntent = new Intent(SplashScreen.this, MainActivity.class);
                        mIntent.putExtra("FROM_ACTIVITY", "A");
                        startActivity(mIntent);
                        finish();
                    } else {
                        // Do something after 5s = 5000ms
                        startActivity(new Intent(SplashScreen.this, LoginActivity.class));
                        finish();
                    }
                }
            }, 2000);

        }
    }

    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else return false;
        } else
            return false;
    }

    public AlertDialog.Builder buildDialog(Context c) {
        builder = new AlertDialog.Builder(c).setCancelable(false);
                builder.setTitle("No Internet Connection");
                builder.setIcon(android.R.drawable.ic_dialog_alert);
                builder.setMessage("Turn on Mobile Data or Wifi to proceed.");

                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        finish();
                    }
                });
        return builder;
    }
}
