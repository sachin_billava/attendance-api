package com.jankariportal;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.meg7.widget.CircleImageView;
import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.List;
import java.util.Objects;


public class LogAdapter extends RecyclerView.Adapter<LogAdapter.LogHolder> {
    private List<AttendanceLog> logs;
    private URL url = null;
    Context context;

    public class LogHolder extends RecyclerView.ViewHolder {
        public TextView date, timeIn, timeOut, address1, address2,name;
        public CircleImageView imageIn, imageOut;

        public LogHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.logName);
            date = itemView.findViewById(R.id.dateLog);
            timeIn = itemView.findViewById(R.id.timeInLog);
            timeOut = itemView.findViewById(R.id.timeOutLog);
            address1 = itemView.findViewById(R.id.inAddress);
            address2 = itemView.findViewById(R.id.outAddress);
            imageIn = itemView.findViewById(R.id.inImage);
            imageOut = itemView.findViewById(R.id.outImage);
        }
    }

    public LogAdapter(List<AttendanceLog> logs) {
        this.logs = logs;
    }

    @NonNull
    @Override
    public LogHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_logs, parent, false);
        context = parent.getContext();
        return new LogHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final LogHolder holder, int position) {
        AttendanceLog attendanceLog = logs.get(position);

        if (attendanceLog.getName() != null){
            holder.name.setText(attendanceLog.getName());
        }else{
            holder.name.setVisibility(View.GONE);
        }
        holder.date.setText(attendanceLog.getDate());
        holder.timeIn.setText(attendanceLog.getTimeIn());
        if (attendanceLog.getTimeOut().equals("null")) {
            holder.timeOut.setText("");
        } else {
            holder.timeOut.setText(attendanceLog.getTimeOut());
        }
        holder.address1.setText(attendanceLog.getAddress1());
        holder.address2.setText(attendanceLog.getAddress2());
        holder.address1.setSelected(true);
        holder.address2.setSelected(true);
        String picin = attendanceLog.getImgInUrl();
        String picout = attendanceLog.getImgOutUrl();

        Picasso.get().load(context.getResources().getString(R.string.domain_url) + context.getResources().getString(R.string.get_attendanceinpic_path) + picin).fit().noFade()
                .into(holder.imageIn);
        holder.imageIn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                openImage(holder.imageIn);
            }
        });
        Picasso.get().load(context.getResources().getString(R.string.domain_url) + context.getResources().getString(R.string.get_attendanceoutpic_path) + picout).fit().noFade()
                .into(holder.imageOut);
        holder.imageOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImage(holder.imageOut);

            }
        });
    }

    @Override
    public int getItemCount() {
        return logs.size();
    }

    private void openImage(ImageView view){
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final View dialogLayout = LayoutInflater.from(context).inflate(R.layout.image_dialog,null);
        ImageView imageView = dialogLayout.findViewById(R.id.dailogImage);

        imageView.setImageDrawable(view.getDrawable());

        builder.setView(dialogLayout);
        builder.setCancelable(true);
        builder.show();

    }
}
