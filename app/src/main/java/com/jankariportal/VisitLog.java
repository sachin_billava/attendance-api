package com.jankariportal;



public class VisitLog {

    private String clientName,addressIn,addressOut,timeIn,timeOut,inImage,outImage;

    public VisitLog(String clientName, String addressIn, String addressOut, String timeIn, String timeOut, String inImage, String outImage) {
        this.clientName = clientName;
        this.addressIn = addressIn;
        this.addressOut = addressOut;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
        this.inImage = inImage;
        this.outImage = outImage;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getAddressIn() {
        return addressIn;
    }

    public void setAddressIn(String addressIn) {
        this.addressIn = addressIn;
    }

    public String getAddressOut() {
        return addressOut;
    }

    public void setAddressOut(String addressOut) {
        this.addressOut = addressOut;
    }

    public String getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(String timeIn) {
        this.timeIn = timeIn;
    }

    public String getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }

    public String getInImage() {
        return inImage;
    }

    public void setInImage(String inImage) {
        this.inImage = inImage;
    }

    public String getOutImage() {
        return outImage;
    }

    public void setOutImage(String outImage) {
        this.outImage = outImage;
    }
}
