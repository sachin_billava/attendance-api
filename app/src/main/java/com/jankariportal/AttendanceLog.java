package com.jankariportal;

public class AttendanceLog {
//    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    private String name;
    private String date;
    private String address1,address2;
    private String imgInUrl,imgOutUrl;
    private String timeIn;
    private String timeOut;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AttendanceLog(String name, String date, String address1, String address2, String imgInUrl, String imgOutUrl, String timeIn, String timeOut) {
        this.name = name;
        this.date = date;
        this.address1 = address1;
        this.address2 = address2;
        this.imgInUrl = imgInUrl;
        this.imgOutUrl = imgOutUrl;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
    }

    public AttendanceLog(String date, String address1, String address2, String imgInUrl, String imgOutUrl, String timeIn, String timeOut) {
//        StrictMode.setThreadPolicy(policy);
        this.date = date;
        this.address1 = address1;
        this.address2 = address2;
        this.imgInUrl = imgInUrl;
        this.imgOutUrl = imgOutUrl;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getImgInUrl() {
        return imgInUrl;
    }

    public void setImgInUrl(String imgInUrl) {
        this.imgInUrl = imgInUrl;
    }

    public String getImgOutUrl() {
        return imgOutUrl;
    }

    public void setImgOutUrl(String imgOutUrl) {
        this.imgOutUrl = imgOutUrl;
    }

    public String getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(String timeIn) {
        this.timeIn = timeIn;
    }

    public String getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }
}
